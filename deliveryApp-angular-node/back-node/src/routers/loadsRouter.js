const express = require('express');

const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddleware.js');

const { allowAccess } = require('../tools/accessControl.js');

const {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateToNextLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingInfoById,
} = require('../controllers/loadsController.js');

router
  .route('/')
  .get(authMiddleware, getUserLoads)
  .post(authMiddleware, allowAccess('createOwn', 'loads'), addUserLoad);

router.get('/active', authMiddleware, allowAccess('readOwn', 'activeLoad'), getUserActiveLoad);

router.patch('/active/state', authMiddleware, allowAccess('updateOwn', 'activeLoad'), iterateToNextLoadState);

router
  .route('/:id')
  .get(authMiddleware, allowAccess('readOwn', 'loads'), getUserLoadById)
  .put(authMiddleware, allowAccess('updateOwn', 'loads'), updateUserLoadById)
  .delete(authMiddleware, allowAccess('deleteOwn', 'loads'), deleteUserLoadById);

router.post('/:id/post', authMiddleware, allowAccess('updateOwn', 'loads'), postUserLoadById);

router.get('/:id/shipping_info', authMiddleware, allowAccess('readOwn', 'loads'), getUserLoadShippingInfoById);

module.exports = {
  loadsRouter: router,
};
