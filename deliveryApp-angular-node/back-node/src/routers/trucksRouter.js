const express = require('express');

const router = express.Router();
const { authMiddleware } = require('../middleware/authMiddleware.js');

const { allowAccess } = require('../tools/accessControl.js');

const {
  getUserTrucks,
  addUserTruck,
  getUserTruckById,
  updateUserTrauckById,
  deleteUserTruckById,
  assignTruckToUser,
} = require('../controllers/trucksController.js');

router
  .route('/')
  .get(authMiddleware, allowAccess('readOwn', 'trucks'), getUserTrucks)
  .post(authMiddleware, allowAccess('createOwn', 'trucks'), addUserTruck);

router
  .route('/:id')
  .get(authMiddleware, allowAccess('readOwn', 'trucks'), getUserTruckById)
  .put(authMiddleware, allowAccess('updateOwn', 'trucks'), updateUserTrauckById)
  .delete(authMiddleware, allowAccess('deleteOwn', 'trucks'), deleteUserTruckById);

router.post('/:id/assign', authMiddleware, allowAccess('updateOwn', 'trucks'), assignTruckToUser);

module.exports = {
  trucksRouter: router,
};
