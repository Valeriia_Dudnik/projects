const express = require('express');

const router = express.Router();
const { createProfile, login, forgotPassword } = require('../controllers/authController.js');
const { passMiddleware } = require('../middleware/passMiddleware.js');

router.post('/register', passMiddleware, createProfile);

router.post('/login', login);

router.post('/forgot_password', forgotPassword);

module.exports = {
  authRouter: router,
};
