/* eslint-disable */
const { getUserInfo, deleteUser, changeUserPassword } = require('../services/usersService.js');

const getProfileInfo = async (req, res) => {
  const { userId } = req.user;
  const profileInfo = await getUserInfo(userId);
  if (!profileInfo) {
    return res.status(400).send({ message: 'User not found' });
  }
  return res.status(200).send({ user: profileInfo });
};

const deleteProfile = async (req, res) => {
  const { userId } = req.user;
  const deleted = await deleteUser(userId);
  if (!deleted) {
    return res.status(400).send({ message: 'User not found' });
  }
  return res.status(200).send({ message: 'Profile deleted successfully' });
};

const changeProfilePassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const { userId } = req.user;

  if (!oldPassword || !newPassword) {
    return res.status(400).send({ message: 'You need to pass OLD password and NEW password' });
  } else {
    const check = await changeUserPassword({ userId, oldPassword, newPassword });
    if (!check) {
      return res.status(400).send({ message: 'Wrong password' }); 
    } else {
      return res.status(200).send({ message: 'Password changed successfully' });
    }
  }
};

module.exports = {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
};
