/* eslint-disable */
const { createUser, loginUser, findUser, createNewPassword } = require('../services/authService.js');

const createProfile = async (req, res) => {
  const { email, password, role } = req.body;
  const user = await findUser(email);
  if (user) {
    return res.status(400).send({ message: 'User already exists' });
  }
  await createUser({ email, password, role });
  return res.status(200).send({ message: 'Success' });
};

const login = async (req, res) => {
  const { email, password } = req.body;
  const { jwtToken, userName } = await loginUser({ email, password });
  if (jwtToken?.message === 'User') {
    return res.status(400).send({ message: 'User not found' });
  } else if (jwtToken?.message === 'Password') {
    return res.status(400).send({ message: 'Wrong password' });
  } else {
    return res.status(200).send({ jwt_token: jwtToken, user_name: userName });}
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;
  const user = await findUser(email);

  if (!user) {
    return res.status(400).send({ message: 'User not found' });
  } else {
    createNewPassword(user);
    res.status(200).send({ message: 'New password sent to your email address' });
  }
};

module.exports = {
  createProfile,
  login,
  forgotPassword,
};
