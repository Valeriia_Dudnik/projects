const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTrauckById,
  deleteTruckById,
  assignTruck,
} = require('../services/trucksService.js');

const getUserTrucks = async (req, res) => {
  const { userId } = req.user;
  const allTrucks = await getTrucks(userId);
  if (!allTrucks) {
    return res.status(400).send({ message: 'Trucks not found' });
  }
  return res.status(200).send({ trucks: allTrucks });
};

const addUserTruck = async (req, res) => {
  const { userId } = req.user;
  const { type } = req.body;

  if (!type) {
    return res.status(400).send({ message: 'Pass type parametr' });
  }
  await addTruck(userId, type);
  return (res.status(200).send({ message: 'Truck created successfully' }));
};

const getUserTruckById = async (req, res) => {
  const { userId } = req.user;
  const truckId = req.params.id;
  const someTruck = await getTruckById(userId, truckId);

  if (!someTruck) {
    return res.status(400).send({ message: 'Truck not found' });
  }
  return res.status(200).send({ truck: someTruck });
};

const updateUserTrauckById = async (req, res) => {
  const { userId } = req.user;
  const truckId = req.params.id;
  const { type } = req.body;
  const someTruck = await updateTrauckById(userId, truckId, type);

  if (!someTruck) {
    return res.status(400).send({ message: 'Truck not found' });
  }
  return res.status(200).send({ message: 'Truck details changed successfully' });
};

const deleteUserTruckById = async (req, res) => {
  const { userId } = req.user;
  const truckId = req.params.id;
  const someTruck = await deleteTruckById(userId, truckId);

  if (!someTruck) {
    return res.status(400).send({ message: 'Truck not found' });
  }
  return res.status(200).send({ message: 'Truck deleted successfully' });
};

const assignTruckToUser = async (req, res) => {
  const { userId } = req.user;
  const truckId = req.params.id;
  const someTruck = await assignTruck(userId, truckId);

  if (!someTruck) {
    return res.status(400).send({ message: 'Truck not found' });
  }
  return res.status(200).send({ message: 'Truck assigned successfully' });
};

module.exports = {
  getUserTrucks,
  addUserTruck,
  getUserTruckById,
  updateUserTrauckById,
  deleteUserTruckById,
  assignTruckToUser,
};
