require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URI);

const { authRouter } = require('./routers/authRouter.js');
const { usersRouter } = require('./routers/usersRouter.js');
const { trucksRouter } = require('./routers/trucksRouter.js');
const { loadsRouter } = require('./routers/loadsRouter.js');
// const { authMiddleware } = require('./middleware/authMiddleware.js');

app.use(cors());
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    app.listen(process.env.PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
function errorHandler(err, req, res) {
  return res.status(500).send({ message: err.message });
}
app.use(errorHandler);
