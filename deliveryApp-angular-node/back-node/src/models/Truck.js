const mongoose = require('mongoose');

const truckSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Types.ObjectId,
    required: false,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

const Truck = mongoose.model('Truck', truckSchema);

module.exports = {
  Truck,
};
