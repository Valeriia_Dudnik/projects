const mongoose = require('mongoose');
const { loadStatus, states } = require('../tools/const.js');

const loadSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Types.ObjectId,
  },
  status: {
    type: String,
    enum: Object.keys(loadStatus),
    default: loadStatus.NEW,
    required: true,
  },
  state: {
    type: String,
    enum: states,
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    width: {
      type: String,
      required: true,
    },
    height: {
      type: String,
      required: true,
    },
    length: {
      type: String,
      required: true,
    },
    required: true,
  },
  logs:
    [{
      message: String,
      time: {
        type: Date,
        default: Date.now(),
      },
    }],
  created_date: {
    type: String,
    default: Date.now(),
  },
});

const Load = mongoose.model('Load', loadSchema);

module.exports = {
  Load,
};
