class InvalidRequest extends Error {
  constructor(message = '', ...args) {
    super(message, ...args);
    this.message = `Error: ${message}`;
  }
}

module.exports = {
  InvalidRequest,
};
