const { Truck } = require('../models/Truck.js');

const getTrucks = async (userId) => {
  const allTrucks = await Truck.find(
    { created_by: userId },
    { _v: false },
  );
  return allTrucks;
};

const addTruck = async (userId, type) => {
  await Truck.create({
    created_by: userId,
    type,
  });
};

const getTruckById = async (userId, truckId) => {
  const truck = await Truck.findOne({ created_by: userId, _id: truckId });
  return truck;
};

const updateTrauckById = async (userId, truckId, info) => {
  const truck = await Truck.findOneAndUpdate(
    { created_by: userId, _id: truckId },
    { ...info },
  );
  return truck;
};

const deleteTruckById = async (userId, truckId) => {
  const truck = await Truck.findOneAndDelete({ created_by: userId, _id: truckId });
  return truck;
};

const assignTruck = async (userId, truckId) => {
  const assignedTruck = await Truck.find({ assigned_to: userId });

  if (assignedTruck.length > 0) {
    return { message: 'User has already assigned a truck' };
  }
  const truck = await Truck.findOneAndUpdate(
    { created_by: userId, _id: truckId, assigned_to: { $exists: false } },
    { assigned_to: userId },
  );
  return truck;
};

module.exports = {
  getTrucks,
  addTruck,
  getTruckById,
  updateTrauckById,
  deleteTruckById,
  assignTruck,
};
