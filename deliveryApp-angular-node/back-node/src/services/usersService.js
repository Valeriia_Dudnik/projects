/* eslint-disable */
const bcryptjs = require('bcryptjs');
const { User } = require('../models/User.js');
const { InvalidRequest } = require('../tools/error.js');

const getUserInfo = async (userId) => {
  const userInfo = await User.findOne(
    { _id: userId },
    { __v: false, password: false },
  );
  return userInfo;
};

const deleteUser = async (userId) => {
  const deletedUser = await User.findOneAndDelete({ _id: userId });
  return deletedUser;
};

const changeUserPassword = async ({ userId, oldPassword, newPassword }) => {
  const userProfile = await User.findOne({ _id: userId });
  if (userProfile && await bcryptjs.compare(oldPassword, userProfile.password)) {
    const updatedUser = await User.findOneAndUpdate(
      { _id: userId },
      { password: await bcryptjs.hash(newPassword, 10) },
    );
    return updatedUser;
  } else {
    // throw new InvalidRequest('Wrong password');
    return false;
  }
};

module.exports = {
  getUserInfo,
  deleteUser,
  changeUserPassword,
};
