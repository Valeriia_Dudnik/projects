/* eslint-disable */
const bcryptjs = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { InvalidRequest } = require('../tools/error.js');
const { User } = require('../models/User.js');
const sgMail = require('@sendgrid/mail');

const createUser = async ({ email, password, role }) => {
  const [username, other] = email.split('@');
  await User.create({
    email,
    username,
    password: await bcryptjs.hash(password, 10),
    role,
  });
};

const loginUser = async ({ email, password }) => {
  const user = await User.findOne({ email });

  if (!user) {
    // throw new InvalidRequest('User not found');
    return { message: 'User' };
  }

  if (!(await bcryptjs.compare(password, user.password))) {
    // throw new InvalidRequest('Wrong password');
    return { message: 'Password' };
  }

  const payload = { userId: user._id, email: user.email, role: user.role };
  const jwtToken = jwt.sign(payload, process.env.JWT_KEY);
  return { jwtToken, userName: user.username };
};

const findUser = async (email) => {
  const user = await User.findOne({ email });
  return user;
};

const createNewPassword = async (user) => {
    const newPassword = Math.random().toString(36).substr(2, 9);
    await User.findOneAndUpdate(
      { email: user.email },
      { password: await bcryptjs.hash(newPassword, 10) },
    );
    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: user.email, 
      from: 'valeriia_dudnik@ukr.net',
      subject: 'Forgot password?',
      text: `Your new password is: ${newPassword}`,
    }
    sgMail
      .send(msg)
      .catch((error) => {
        console.error(error)
      })
};

module.exports = {
  createUser,
  loginUser,
  findUser,
  createNewPassword,
};
