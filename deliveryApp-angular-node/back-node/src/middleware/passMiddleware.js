const joi = require('joi');

const passMiddleware = async (req, res, next) => {
  const schema = joi.object({
    password: joi.string()
      .min(4)
      .max(24)
      .required(),
    email: joi.string()
      .email()
      .required(),
    role: joi.string()
      .required(),
  });
  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  passMiddleware,
};
