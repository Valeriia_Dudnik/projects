export const getCoursesState = (state) => state.courses.courses;

export const getUsersState = (state) => state.user.user;
export const getUsersIsAuthState = (state) => state.user.user.isAuth;
export const getUsersRoleState = (state) => state.user.user.role;
export const getUsersNameState = (state) => state.user.user.name;

export const getAuthorsState = (state) => state.authors.authors;
