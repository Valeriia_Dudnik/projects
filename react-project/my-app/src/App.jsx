import React, { useEffect } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CourseForm from './components/CourseForm/CourseForm';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import CourseInfo from './components/CourseInfo/CourseInfo';

import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';

import {
	getUsersIsAuthState,
	getUsersRoleState,
	getUsersNameState,
} from './selectors';

import { fetchCourses, fetchAuthors } from './services';

import './App.css';

import { PrivateRouter } from './components/PrivateRouter/PrivateRouter';
import { ProtectedRoute } from './components/ProtectedRoute/ProtectedRoute';

const App = () => {
	const loggedIn = useSelector(getUsersIsAuthState);
	const loggedRole = useSelector(getUsersRoleState);
	const loggedUserName = useSelector(getUsersNameState);

	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(fetchCourses());
		dispatch(fetchAuthors());
	}, []);

	return (
		<div className='app'>
			<Header loggedUserName={loggedUserName} loggedIn={loggedIn} />
			<main>
				<Routes>
					<Route path='/registration' element={<Registration />} />
					<Route path='/login' element={<Login />} />
					<Route
						path='/courses'
						element={
							<ProtectedRoute loggedIn={loggedIn}>
								<Courses loggedRole={loggedRole} />
							</ProtectedRoute>
						}
					/>
					<Route
						path='/courses/add'
						element={
							<PrivateRouter loggedRole={loggedRole}>
								<CourseForm />
							</PrivateRouter>
						}
					/>
					<Route
						path='/courses/update/:courseId'
						element={
							<PrivateRouter loggedRole={loggedRole}>
								<CourseForm />
							</PrivateRouter>
						}
					/>
					<Route
						path='courses/:courseId'
						element={
							<ProtectedRoute loggedIn={loggedIn}>
								<CourseInfo />
							</ProtectedRoute>
						}
					/>
					<Route
						path='/'
						element={
							<ProtectedRoute loggedIn={loggedIn}>
								<Navigate to='/courses' replace={true} />
							</ProtectedRoute>
						}
					/>
					<Route
						path='*'
						element={
							<ProtectedRoute loggedIn={loggedIn}>
								<Navigate to='/courses' replace={true} />
							</ProtectedRoute>
						}
					/>
				</Routes>
			</main>
		</div>
	);
};

export default App;
