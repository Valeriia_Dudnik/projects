import { getCoursesAction } from './store/courses/actionCreators';
import { getAuthorsAction } from './store/authors/actionCreators';
import { loginUserAction } from './store/user/actionCreators';

const mainUrl = 'http://localhost:4000';

export const fetchCourses = () => {
	return function (dispatch) {
		fetch(mainUrl + '/courses/all')
			.then((response) => response.json())
			.then((json) => {
				dispatch(getCoursesAction(json.result));
			})
			.catch((error) => {
				console.error('There was an error!', error);
			});
	};
};

export const fetchAuthors = () => {
	return function (dispatch) {
		fetch(mainUrl + '/authors/all')
			.then((response) => response.json())
			.then((json) => dispatch(getAuthorsAction(json.result)))
			.catch((error) => {
				console.error('There was an error!', error);
			});
	};
};

export const getLoggedUser = () => {
	return function (dispatch) {
		fetch(mainUrl + '/users/me', {
			headers: {
				Authorization: localStorage.getItem('result'),
			},
		})
			.then((response) => response.json())
			.then((json) => {
				// console.log(json);
				dispatch(loginUserAction(json.result));
			})
			.catch((error) => {
				console.error('There was an error!', error);
			});
	};
};

export async function registerNewUser(newUser) {
	const response = await fetch(mainUrl + '/register', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			// Authorization: localStorage.getItem('result'),
		},
		body: JSON.stringify(newUser),
	});
	return await response.json().then((json) => {
		if (json.successful === false) {
			throw new Error();
		}
	});
}

export async function fetchLogUser(logUser) {
	const response = await fetch(mainUrl + '/login', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(logUser),
	});
	return await response.json();
}

export async function logoutUser() {
	await fetch(mainUrl + '/logout', {
		method: 'DELETE',
		headers: {
			Authorization: localStorage.getItem('result'),
		},
		// }).then((res) => {
		// 	console.log(res);
	});
}

export async function fetchNewCourse(newCourse) {
	const response = await fetch(mainUrl + '/courses/add', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			Authorization: localStorage.getItem('result'),
		},
		body: JSON.stringify(newCourse),
	});
	return await response.json();
}

export async function fetchNewAuthor(newAuthor) {
	const response = await fetch(mainUrl + '/authors/add', {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json',
			Authorization: localStorage.getItem('result'),
		},
		body: JSON.stringify(newAuthor),
	});
	return await response.json();
}

export async function deleteSomeCourse(courseID) {
	await fetch(mainUrl + `/courses/${courseID}`, {
		method: 'DELETE',
		headers: {
			Authorization: localStorage.getItem('result'),
		},
		// }).then((res) => {
		// 	console.log(res);
	});
}

export async function updateSomeCourse(courseID, course) {
	const response = await fetch(mainUrl + `/courses/${courseID}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json',
			Authorization: localStorage.getItem('result'),
		},
		body: JSON.stringify(course),
	});
	return await response.json();
}
