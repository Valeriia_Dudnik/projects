import React from 'react';
import pic from './courses.png';

export default function Logo() {
	return <img src={pic} alt='logo' />;
}
