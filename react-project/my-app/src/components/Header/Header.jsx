import React from 'react';

import Button from '../../common/Button/Button';
import Logo from './components/Logo/Logo';

import { button_logout } from '../../common/Button/constants.js';

import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { logoutSomeUser } from '../../store/user/thunk';

import './header.css';

export default function Header({ loggedUserName, loggedIn }) {
	let navigate = useNavigate();
	const dispatch = useDispatch();

	const logOutUser = () => {
		dispatch(logoutSomeUser())
			.then(() => {
				navigate('/login');
			})
			.catch((error) => {
				console.error('There was an error!', error);
			});
	};

	return (
		<header>
			<Logo />
			{loggedIn ? (
				<div className='user'>
					<h3>{loggedUserName}</h3>
					<Button text={button_logout} clickEvent={logOutUser} />
				</div>
			) : null}
		</header>
	);
}
