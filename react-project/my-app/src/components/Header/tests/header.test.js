import { render, screen } from '@testing-library/react';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import Header from '../Header';

const mockedState = {
	user: {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
	},
	courses: {
		courses: [],
	},
	authors: {
		authors: [],
	},
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

const MockHeader = ({ loggedUserName, loggedIn }) => {
	return (
		<Provider store={mockedStore}>
			<BrowserRouter>
				<Header loggedUserName={loggedUserName} loggedIn={loggedIn} />
			</BrowserRouter>
		</Provider>
	);
};

describe('Header', () => {
	test('should have a logo', () => {
		render(<MockHeader />);
		const logo = screen.getByAltText(/logo/i);
		expect(logo).toBeInTheDocument();
	});

	test('should have a user name', () => {
		render(<MockHeader loggedUserName={'Arty'} loggedIn={true} />);
		const userName = screen.getByText(/Arty/i);
		expect(userName).toBeInTheDocument();
	});
});
