import React, { useState } from 'react';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import { button_login } from '../../common/Button/constants.js';

import { Link } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { logSomeUser } from '../../store/user/thunk';

import './login.css';

const Login = () => {
	let navigate = useNavigate();
	const dispatch = useDispatch();

	const requiredFields = () => {
		alert('Please, fill in all fields');
	};

	const [logUser, setLogUser] = useState({
		email: '',
		password: '',
	});

	const [error, setError] = useState('');

	const loginSomeUser = () => {
		dispatch(logSomeUser(logUser))
			.then(() => {
				setError('');
				navigate('/courses');
			})
			.catch((error) => {
				setError(`This email doesn't exist or password is wrong`);
				console.error('There was an error!', error);
			});
	};

	const clickLogin = (e) => {
		e.preventDefault();
		if (Object.values(logUser).some((el) => el.length === 0)) {
			requiredFields();
		} else {
			loginSomeUser();
		}
	};

	return (
		<form className='logForm' onSubmit={clickLogin}>
			<h2>Login</h2>
			<Input
				id='logEmail'
				placeholder='Enter email'
				labelName='Email'
				type='email'
				value={logUser.email}
				onChange={(e) => setLogUser({ ...logUser, email: e.target.value })}
			/>
			<Input
				id='logPassword'
				placeholder='Enter password'
				labelName='Password'
				type='password'
				value={logUser.password}
				onChange={(e) => setLogUser({ ...logUser, password: e.target.value })}
			/>
			<div className='log'>{error}</div>
			<br />
			<Button text={button_login} />
			<p>
				If you don`t have an account you can{' '}
				<Link to='/registration'>Registration</Link>
			</p>
		</form>
	);
};

export default Login;
