import React from 'react';
import { Navigate } from 'react-router-dom';

export const PrivateRouter = ({ loggedRole, children }) => {
	return loggedRole === 'admin' ? (
		children
	) : (
		<Navigate to='/courses' replace={true} />
	);
};
