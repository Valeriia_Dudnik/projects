import React, { useState, useEffect } from 'react';

import Button from '../../common/Button/Button';
import { button_goBack } from '../../common/Button/constants';
import { useParams } from 'react-router-dom';

import { Link } from 'react-router-dom';

import dateGenerator from '../../helpers/dateGenerator';
import pipeDuration from '../../helpers/pipeDuration';

import { useSelector } from 'react-redux';
import { getCoursesState, getAuthorsState } from '../../selectors';

import './courseInfo.css';

const CourseInfo = () => {
	const courses = useSelector(getCoursesState);
	const authors = useSelector(getAuthorsState);

	const [courseName, setCourseName] = useState('title');
	const [courseDescription, setCourseDescription] = useState('discription');
	const [courseIdent, setCourseIdent] = useState('id');
	const [courseDuration, setCourseDuration] = useState(0);
	const [courseCreated, setCourseCreated] = useState('creating date');
	const [courseAuthors, setCourseAuthors] = useState([]);

	const { courseId } = useParams();

	useEffect(() => {
		const newCourse = courses.find((el) => el.id === courseId);
		setCourseIdent(newCourse.id);
		setCourseName(newCourse.title);
		setCourseDescription(newCourse.description);
		setCourseDuration(newCourse.duration);
		setCourseCreated(newCourse.creationDate);
		setCourseAuthors(newCourse.authors);
	}, [courses, courseId]);

	const findAuthor = () => {
		const authorsList = authors.filter((o1) =>
			courseAuthors.some((o2) => o1.id === o2)
		);
		const authorsName = authorsList.map((el) => el.name);

		return authorsName.map((el) => <p key={el}>{el}</p>);
	};

	return (
		<div className='courseInfoS'>
			<Link to='/courses'>
				<Button text={button_goBack} />
			</Link>
			<div className='courseInfoStyle'>
				<h2>{courseName}</h2>

				<div className='courseInfoMain'>
					<p>{courseDescription}</p>

					<div>
						<p>
							<b>ID: </b>
							{courseIdent}
						</p>
						<p>
							<b>Duration: </b>
							{pipeDuration(courseDuration)}
						</p>
						<p>
							<b>Created: </b>
							{dateGenerator(courseCreated)}
						</p>
						<div>
							<b>Authors: </b>
							{findAuthor()}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CourseInfo;
