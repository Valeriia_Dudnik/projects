import { render, screen, fireEvent } from '@testing-library/react';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import CoursesForm from '../CourseForm';

const mockedAuthorsList = [
	{
		id: '27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		name: 'Vasiliy Dobkin',
	},
];

const mockedState = {
	user: {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
	},
	courses: {
		courses: [],
	},
	authors: {
		authors: mockedAuthorsList,
	},
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

const MockCoursesForm = () => {
	return (
		<Provider store={mockedStore}>
			<BrowserRouter>
				<CoursesForm />
			</BrowserRouter>
		</Provider>
	);
};

describe('CoursesForm', () => {
	test('should show authors lists (all and course authors)', () => {
		render(<MockCoursesForm />);
		const allAuthorsList = screen.getByTestId(/allAuthors/i);
		const courseAuthorsList = screen.getByTestId(/courseAuthors/i);
		expect(allAuthorsList).toBeInTheDocument();
		expect(courseAuthorsList).toBeInTheDocument();
	});

	test(`'Create author' click button should call dispatch`, () => {
		render(<MockCoursesForm />);
		const buttonCreateAuthor = screen.getByRole('button', {
			name: /Create author/i,
		});
		const newAuthorInput = screen.getByPlaceholderText(/Enter author name.../i);
		fireEvent.change(newAuthorInput, { target: { value: 'Mark Zuckerberg' } });
		fireEvent.click(buttonCreateAuthor);
		expect(mockedStore.dispatch).toHaveBeenCalled();
	});

	test(`'Add author' button click should add an author to course authors list`, () => {
		render(<MockCoursesForm />);
		const buttonAddAuthor = screen.getByRole('button', {
			name: /Add/i,
		});
		fireEvent.click(buttonAddAuthor);
		const courseAuthors = screen.getByTestId(/courseAuthors/i);
		const selectedAuthor = screen.getByText(/Vasiliy Dobkin/i);
		expect(courseAuthors).toContainElement(selectedAuthor);
	});

	test(`'Delete author' button click should delete an author from the course list`, () => {
		render(<MockCoursesForm />);
		const buttonAddAuthor = screen.getByRole('button', {
			name: /Add/i,
		});
		fireEvent.click(buttonAddAuthor);
		const buttonDeleteAuthor = screen.getByRole('button', {
			name: /Delete/i,
		});
		fireEvent.click(buttonDeleteAuthor);
		const courseAuthors = screen.getByTestId(/courseAuthors/i);
		const selectedAuthor = screen.getByText(/Vasiliy Dobkin/i);
		expect(courseAuthors).not.toContainElement(selectedAuthor);
	});
});
