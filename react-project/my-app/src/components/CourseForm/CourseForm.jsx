import React, { useEffect, useState } from 'react';

import CreateAuthor from './components/CreateAuthor';
import AllAuthorsList from './components/AllAuthorsList';
import NewCourseAuthors from './components/NewCourseAuthors';
import CreateNewCourse from './components/CreateNewCourse';
import Input from '../../common/Input/Input';
import { useParams } from 'react-router-dom';

import pipeDuration from '../../helpers/pipeDuration';

import { useSelector } from 'react-redux';
import { getCoursesState, getAuthorsState } from '../../selectors';

import './courseForm.css';

export default function CourseForm() {
	const { courseId } = useParams();

	const courses = useSelector(getCoursesState);
	const authors = useSelector(getAuthorsState);

	const [updatedCourse, setUpdatedCourse] = useState({
		title: '',
		description: '',
		duration: 0,
		authors: [],
	});

	useEffect(() => {
		setUpdatedCourse(courses.find((el) => el.id === courseId));
	}, [courseId, courses]);

	const updateTitle = (e) => {
		setUpdatedCourse({ ...updatedCourse, title: e.target.value });
	};

	const updateDescription = (e) => {
		setUpdatedCourse({ ...updatedCourse, description: e.target.value });
	};

	const [value, setValue] = useState();

	const [allAuthorsList, setAllAuthorsList] = useState(authors);

	useEffect(() => {
		setAllAuthorsList(authors);
	}, [authors]);

	const [courseAuthorsList, setCourseAuthorsList] = useState([]);

	const createCoursePost = (post) => {
		setCourseAuthorsList([...courseAuthorsList, post]);
		setAllAuthorsList(allAuthorsList.filter((p) => p.id !== post.id));
	};

	const removeCoursePost = (post) => {
		setAllAuthorsList([...allAuthorsList, post]);
		setCourseAuthorsList(courseAuthorsList.filter((p) => p.id !== post.id));
	};

	const [authorCourse, setAuthorCourse] = useState([]);
	const [authorList, setAuthorList] = useState([]);

	useEffect(() => {
		updatedCourse
			? setAuthorCourse(
					authors.filter((o1) =>
						updatedCourse.authors.some((o2) => o1.id === o2)
					)
			  )
			: console.log('hello');
	}, [updatedCourse]);

	useEffect(() => {
		setAuthorList(
			authors.filter((o1) => !authorCourse.some((o2) => o1.id === o2.id))
		);
	}, [authors, authorCourse]);

	const createAuthorCourse = (post) => {
		setAuthorCourse([...authorCourse, post]);
		setAuthorList(authorList.filter((p) => p.id !== post.id));
	};

	const removeAuthorList = (post) => {
		setAuthorList([...authorList, post]);
		setAuthorCourse(authorCourse.filter((p) => p.id !== post.id));
	};

	return (
		<form className='formCreate'>
			<CreateNewCourse
				durationValue={value}
				courseAuthorsList={courseAuthorsList}
				updatedCourse={updatedCourse}
				courseId={courseId}
				updateTitle={updateTitle}
				updateDescription={updateDescription}
				authorCourse={authorCourse}
			/>
			<div className='newCourse'>
				<CreateAuthor />
				<AllAuthorsList
					movePost={createCoursePost}
					createAuthorCourse={createAuthorCourse}
					allAuthorsList={allAuthorsList}
					authorList={authorList}
					courseId={courseId}
				/>
				{courseId ? (
					<div className='newCourseduration'>
						<h3>Duration</h3>
						<Input
							id='duration'
							placeholder='Enter duration in minutes...'
							labelName='Duration'
							type='number'
							value={updatedCourse.duration}
							onChange={(e) =>
								setUpdatedCourse({
									...updatedCourse,
									duration: Number(e.target.value),
								})
							}
						/>
						<h2>Duration: {pipeDuration(updatedCourse.duration)}</h2>
					</div>
				) : (
					<div className='newCourseduration'>
						<h3>Duration</h3>
						<Input
							id='duration'
							placeholder='Enter duration in minutes...'
							labelName='Duration'
							type='number'
							value={value}
							onChange={(e) => setValue(e.target.value)}
						/>
						<h2>Duration: {pipeDuration(value)}</h2>
					</div>
				)}
				<NewCourseAuthors
					removeCoursePost={removeCoursePost}
					removeAuthorList={removeAuthorList}
					courseAuthorsList={courseAuthorsList}
					authors={authorCourse}
					courseId={courseId}
				/>
			</div>
		</form>
	);
}
