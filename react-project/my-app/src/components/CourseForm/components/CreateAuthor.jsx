import React, { useState } from 'react';

import Button from '../../../common/Button/Button';
import { button_createAuthor } from '../../../common/Button/constants.js';
import Input from '../../../common/Input/Input';

import { useDispatch } from 'react-redux';
import { addAuthor } from '../../../store/authors/thunk';

const CreateAuthor = () => {
	const dispatch = useDispatch();
	const [post, setPost] = useState({ name: '' });

	const addNewAuthor = (e) => {
		e.preventDefault();
		if (post.name && post.name.length >= 2) {
			dispatch(addAuthor(post));
			setPost({ name: '' });
		}
	};

	return (
		<div className='newCourseAddAuthor'>
			<h3>Add author</h3>
			<Input
				id='courseAutor'
				placeholder='Enter author name...'
				labelName='Author name'
				value={post.name}
				onChange={(e) => setPost({ ...post, name: e.target.value })}
			/>
			<Button clickEvent={addNewAuthor} text={button_createAuthor} />
		</div>
	);
};
export default CreateAuthor;
