import React from 'react';

import Button from '../../../common/Button/Button';
import { button_add } from '../../../common/Button/constants.js';

export default function CreateAuthorElement(props) {
	return (
		<li className='authorsLi'>
			<p className='authorsName'>{props.post.name}</p>
			<Button
				clickEvent={(e) => {
					e.preventDefault();
					props.movePost(props.post);
				}}
				text={button_add}
			/>
		</li>
	);
}
