import React from 'react';
import CreateCourseAuthor from './CreateCourseAuthor';

import PropTypes from 'prop-types';

export default function NewCourseAuthors({
	courseAuthorsList,
	removeCoursePost,
	authors,
	courseId,
	removeAuthorList,
}) {
	return (
		<div className='newCourseAuthors'>
			<h3>Course authors</h3>
			{courseId ? (
				<ul data-testid='courseAuthors' className='newCourseList'>
					{authors.length !== 0 ? (
						authors.map((post) => (
							<CreateCourseAuthor
								removeCoursePost={removeAuthorList}
								post={post}
								key={post.id}
							/>
						))
					) : (
						<div className='empty'>Author list is empty</div>
					)}
				</ul>
			) : (
				<ul data-testid='courseAuthors' className='newCourseList'>
					{courseAuthorsList.length !== 0 ? (
						courseAuthorsList.map((post) => (
							<CreateCourseAuthor
								removeCoursePost={removeCoursePost}
								post={post}
								key={post.id}
							/>
						))
					) : (
						<div className='empty'>Author list is empty</div>
					)}
				</ul>
			)}
		</div>
	);
}

NewCourseAuthors.propTypes = {
	courseAuthorsList: PropTypes.array,
};
