import React, { useEffect, useState } from 'react';

import Button from '../../../common/Button/Button';
import {
	button_createCourse,
	button_updateCourse,
} from '../../../common/Button/constants.js';
import Input from '../../../common/Input/Input';

import { useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';
import { addCourse, updateCourse } from '../../../store/courses/thunk';

const CreateNewCourse = ({
	durationValue,
	courseAuthorsList,
	updatedCourse,
	courseId,
	updateTitle,
	updateDescription,
	authorCourse,
}) => {
	const dispatch = useDispatch();

	const requiredFields = () => {
		alert('Please, fill in all fields!');
	};

	const requiredLength = () => {
		alert('Course description should be more then 2 symbols!');
	};

	const requiredDuration = () => {
		alert('Course duration shold be more then 00:00 hours!');
	};

	let navigate = useNavigate();

	const [course, setCourse] = useState({
		title: '',
		description: '',
		duration: 0,
		authors: '',
	});

	function addCourseNew(course) {
		dispatch(addCourse(course))
			.then(() => {
				navigate('/courses');
			})
			.catch((error) => {
				console.error('There was an error!', error);
			});
	}

	useEffect(() => {
		setCourse({
			...course,
			duration: Number(durationValue),
			authors: courseAuthorsList.map((el) => el.id),
		});
	}, [durationValue, courseAuthorsList]);

	const addNewCourse = (e) => {
		e.preventDefault();
		let elementValues = Object.values(course);
		if (elementValues.some((el) => el.toString().length === 0)) {
			requiredFields();
		} else if (course.description.length < 2) {
			requiredLength();
		} else if (durationValue < 1) {
			requiredDuration();
		} else {
			addCourseNew(course);
			setCourse({
				title: '',
				description: '',
			});
		}
	};

	function updatedSelectedCourse(e) {
		e.preventDefault();
		updatedCourse.authors = authorCourse.map((el) => el.id);
		let elementValues = Object.values(updatedCourse);
		if (elementValues.some((el) => el.toString().length === 0)) {
			requiredFields();
		} else {
			dispatch(updateCourse(courseId, updatedCourse))
				.then(() => {
					navigate('/courses');
				})
				.catch((error) => {
					console.error('There was an error!', error);
				});
		}
	}

	return (
		<div>
			{courseId ? (
				<div className='course'>
					<Input
						className='courseTitle'
						id='courseTitle'
						placeholder='Enter title...'
						labelName='Title'
						value={updatedCourse.title}
						onChange={(e) => updateTitle(e)}
					/>
					<Button
						clickEvent={updatedSelectedCourse}
						text={button_updateCourse}
					/>
				</div>
			) : (
				<div className='course'>
					<Input
						className='courseTitle'
						id='courseTitle'
						placeholder='Enter title...'
						labelName='Title'
						value={course.title}
						onChange={(e) => setCourse({ ...course, title: e.target.value })}
					/>
					<Button clickEvent={addNewCourse} text={button_createCourse} />
				</div>
			)}
			<label htmlFor='courseDescription'>Description</label> <br />
			{courseId ? (
				<textarea
					className='courseDescription'
					id='courseDescription'
					placeholder='Enter description...'
					value={updatedCourse.description}
					onChange={(e) => updateDescription(e)}
				/>
			) : (
				<textarea
					className='courseDescription'
					id='courseDescription'
					placeholder='Enter description...'
					value={course.description}
					onChange={(e) =>
						setCourse({ ...course, description: e.target.value })
					}
				/>
			)}
		</div>
	);
};

CreateNewCourse.propTypes = {
	durationValue: PropTypes.string,
};

export default CreateNewCourse;
