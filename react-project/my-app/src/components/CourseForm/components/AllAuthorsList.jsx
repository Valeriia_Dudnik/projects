import React from 'react';
import CreateAuthorElement from './CreateAuthorElement';

export default function AllAuthorsList({
	movePost,
	allAuthorsList,
	authorList,
	courseId,
	createAuthorCourse,
}) {
	return (
		<div>
			<h3>Authors</h3>
			{courseId ? (
				<ul data-testid='allAuthors' className='authorsList'>
					{authorList.map((post) => (
						<CreateAuthorElement
							movePost={createAuthorCourse}
							post={post}
							key={post.id}
						/>
					))}
				</ul>
			) : (
				<ul data-testid='allAuthors' className='authorsList'>
					{allAuthorsList.map((post) => (
						<CreateAuthorElement
							movePost={movePost}
							post={post}
							key={post.id}
						/>
					))}
				</ul>
			)}
		</div>
	);
}
