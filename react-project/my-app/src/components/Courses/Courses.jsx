import React, { useEffect, useMemo, useState } from 'react';

import Button from '../../common/Button/Button';
import CourseCard from './components/CourseCard/CourseCard';
import SearchBar from './components/SearchBar/SearchBar';

import { button_addNewCourse } from '../../common/Button/constants.js';
import { Link } from 'react-router-dom';

import { useSelector, useDispatch } from 'react-redux';
import { getCoursesState, getAuthorsState } from '../../selectors';
import { getLoggedUser } from '../../services';
import './courses.css';

export default function Courses({ loggedRole }) {
	const dispatch = useDispatch();

	const courses = useSelector(getCoursesState);
	const authors = useSelector(getAuthorsState);

	useEffect(() => {
		dispatch(getLoggedUser());
	}, []);

	const [searchQuery, setSearchQuery] = useState('');
	const [inputQuery, setInputQuery] = useState('');

	const searchedPosts = useMemo(() => {
		if (inputQuery) {
			const selectedCoursesTitle = courses.filter((post) =>
				post.title.toLocaleLowerCase().includes(inputQuery.toLocaleLowerCase())
			);
			const selectedCoursesID = courses.filter((post) =>
				post.id.toLocaleLowerCase().includes(inputQuery.toLocaleLowerCase())
			);
			const result = selectedCoursesTitle.concat(selectedCoursesID);
			return [...new Set(result)];
		} else {
			return courses;
		}
	}, [courses, inputQuery]);

	const setQuery = (e) => {
		e.preventDefault();
		setSearchQuery(e.target.value);
	};

	const search = (e) => {
		e.preventDefault();
		setInputQuery(searchQuery);
	};

	return (
		<main className='coursesBlock'>
			<section className='searchBlock'>
				<SearchBar value={searchQuery} onChange={setQuery} onClick={search} />
				{loggedRole === 'admin' ? (
					<Link to='/courses/add'>
						<Button text={button_addNewCourse} />
					</Link>
				) : null}
			</section>
			<section data-testid='coursesSection'>
				{searchedPosts.map((card) => {
					const authorsList = authors.filter((o1) =>
						card.authors.some((o2) => o1.id === o2)
					);
					const authorsName = authorsList.map((el) => el.name);

					return (
						<CourseCard
							card={card}
							authors={authorsName.join(', ')}
							key={card.id}
							loggedRole={loggedRole}
						/>
					);
				})}
			</section>
		</main>
	);
}
