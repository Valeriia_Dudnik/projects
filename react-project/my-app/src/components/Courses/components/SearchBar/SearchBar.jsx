import React from 'react';

import Input from '../../../../common/Input/Input';
import Button from '../../../../common/Button/Button';
import { button_search } from '../../../../common/Button/constants';

import PropTypes from 'prop-types';

import './searchBar.css';

export default function SearchBar({ value, onChange, onClick }) {
	return (
		<div className='searchBar'>
			<Input
				value={value}
				onChange={onChange}
				placeholder='Enter course name or id...'
				id='inputCourse'
			/>
			<Button text={button_search} clickEvent={onClick} />
		</div>
	);
}

SearchBar.propTypes = {
	value: PropTypes.string,
};
