import React from 'react';

import Button from '../../../../common/Button/Button';
import { button_showCourse } from '../../../../common/Button/constants.js';

import dateGenerator from '../../../../helpers/dateGenerator';
import pipeDuration from '../../../../helpers/pipeDuration';

import { Link } from 'react-router-dom';

import { useDispatch } from 'react-redux';
import { deleteCourse } from '../../../../store/courses/thunk';

import './courseCard.css';

export default function CourseCard(props) {
	const dispatch = useDispatch();

	const deleteSelectedCourse = (card) => {
		dispatch(deleteCourse(card.id))
			.then(() => {
				// console.log('1');
			})
			.catch((error) => {
				console.error('There was an error!', error);
			});
	};

	return (
		<article>
			<div className='courseTitl'>
				<h2>{props.card.title}</h2>
				<p>{props.card.description}</p>
			</div>
			<div className='courseInfo'>
				<div>
					<p className='lineBreak'>
						<b>Authors: </b>
						{props.authors}
					</p>
					<p>
						<b>Duration: </b>
						{pipeDuration(props.card.duration)}
					</p>
					<p>
						<b>Created: </b>
						{dateGenerator(props.card.creationDate)}
					</p>
				</div>
				<div className='buttonBlock'>
					<Link
						style={{ textDecoration: 'none' }}
						to={`/courses/${props.card.id}`}
					>
						<Button text={button_showCourse} />
					</Link>
					{props.loggedRole === 'admin' ? (
						<div>
							<Link
								style={{ textDecoration: 'none' }}
								to={`/courses/update/${props.card.id}`}
							>
								<Button className='updateButton' />
							</Link>
							<Button
								className='deleteButton'
								clickEvent={() => deleteSelectedCourse(props.card)}
							/>
						</div>
					) : null}
				</div>
			</div>
		</article>
	);
}
