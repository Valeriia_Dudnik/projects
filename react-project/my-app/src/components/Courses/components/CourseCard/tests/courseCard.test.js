import { render, screen } from '@testing-library/react';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import CourseCard from '../CourseCard';

const mockedState = {
	user: {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
	},
	courses: {
		courses: [],
	},
	authors: {
		authors: [],
	},
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

const MockCourseCard = ({ card, authors }) => {
	return (
		<Provider store={mockedStore}>
			<BrowserRouter>
				<CourseCard card={card} authors={authors} />
			</BrowserRouter>
		</Provider>
	);
};

const cardData = {
	title: 'JS',
	description: 'learning is good',
	duration: 1020,
	creationDate: '27/05/2022',
};

const authorsData = ['author', 'author2'];

describe('CourseCard', () => {
	test('should display title', () => {
		render(<MockCourseCard card={cardData} />);
		const courseTitle = screen.getByText(/js/i);
		expect(courseTitle).toBeInTheDocument();
	});

	test('should display description', () => {
		render(<MockCourseCard card={cardData} />);
		const courseDescription = screen.getByText(/learning is good/i);
		expect(courseDescription).toBeInTheDocument();
	});

	test('should display duration in the correct format', () => {
		render(<MockCourseCard card={cardData} />);
		const courseDuration = screen.getByText(/17:00 hours/i);
		expect(courseDuration).toBeInTheDocument();
	});

	test('should display authors list', () => {
		render(<MockCourseCard card={cardData} authors={authorsData.join(', ')} />);
		const courseAuthors = screen.getByText(/author, author2/i);
		expect(courseAuthors).toBeInTheDocument();
	});

	test('should display created date in the correct format', () => {
		render(<MockCourseCard card={cardData} />);
		const courseDate = screen.getByText(/27.05.2022/i);
		expect(courseDate).toBeInTheDocument();
	});
});
