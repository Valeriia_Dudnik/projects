import { render, screen, fireEvent } from '@testing-library/react';

import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import Courses from '../Courses';

const mockedCoursesList = [
	{
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum is simply dummy text of the printing and
typesetting industry. Lorem Ipsum
                    has been the industry's standard dummy text ever since the
1500s, when an unknown
                    printer took a galley of type and scrambled it to make a type
specimen book. It has survived
                    not only five centuries, but also the leap into electronic
typesetting, remaining essentially u
                    nchanged.`,
		creationDate: '8/3/2021',
		duration: 160,
		authors: [
			'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
			'f762978b-61eb-4096-812b-ebde22838167',
		],
	},
	{
		id: 'b5630fdd-7bf7-4d39-b75a-2b5906fd0916',
		title: 'Angular',
		description: `Lorem Ipsum is simply dummy text of the printing and
typesetting industry. Lorem Ipsum
                    has been the industry's standard dummy text ever since the
1500s, when an unknown
                    printer took a galley of type and scrambled it to make a type
specimen book.`,
		creationDate: '10/11/2020',
		duration: 210,
		authors: [
			'df32994e-b23d-497c-9e4d-84e4dc02882f',
			'095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		],
	},
];

const mockedAuthorsList = [
	{
		id: '27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		name: 'Vasiliy Dobkin',
	},
	{
		id: 'f762978b-61eb-4096-812b-ebde22838167',
		name: 'Nicolas Kim',
	},
	{
		id: 'df32994e-b23d-497c-9e4d-84e4dc02882f',
		name: 'Anna Sidorenko',
	},
	{
		id: '095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		name: 'Valentina Larina',
	},
];

const mockedState = {
	user: {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
	},
	courses: {
		courses: mockedCoursesList,
	},
	authors: {
		authors: mockedAuthorsList,
	},
};

const mockedEmptyState = {
	user: {
		user: {
			isAuth: true,
			name: 'Test Name',
		},
	},
	courses: {
		courses: [],
	},
	authors: {
		authors: [],
	},
};

const mockedStore = {
	getState: () => mockedState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

const mockedEmptyStore = {
	getState: () => mockedEmptyState,
	subscribe: jest.fn(),
	dispatch: jest.fn(),
};

const MockCourses = ({ loggedRole }) => {
	return (
		<Provider store={mockedStore}>
			<BrowserRouter>
				<Courses loggedRole={loggedRole} />
			</BrowserRouter>
		</Provider>
	);
};

const MockEmptyCourses = () => {
	return (
		<Provider store={mockedEmptyStore}>
			<BrowserRouter>
				<Courses />
			</BrowserRouter>
		</Provider>
	);
};

describe('Courses', () => {
	test('should display amount of CourseCard equal length of courses array', () => {
		render(<MockCourses />);
		const coursesCards = screen.getAllByRole('article');
		expect(coursesCards.length).toBe(mockedCoursesList.length);
	});

	test('should display Empty container if courses array length is 0', () => {
		render(<MockEmptyCourses />);
		const coursesContainer = screen.getByTestId('coursesSection');
		expect(coursesContainer).toBeEmptyDOMElement();
	});
});

test('CourseForm should be showed after a click on a button "Add new course"', () => {
	render(<MockCourses loggedRole={'admin'} />);
	const buttonAdd = screen.getByRole('button', {
		name: /Add new course/i,
	});
	fireEvent.click(buttonAdd);
	expect(global.window.location.pathname).toEqual('/courses/add');
});
