import * as actions from './actionTypes';

export const loginUserAction = (payload) => ({
	type: actions.LOGIN_USER,
	payload,
});

export const logoutUserAction = (payload) => ({
	type: actions.LOGOUT_USER,
	payload,
});
