import * as actions from './actionTypes';

const userInitialState = {
	user: {
		isAuth: false,
		name: '',
		email: '',
		role: '',
		token: '',
	},
};

export default function userReducer(state = userInitialState, action) {
	switch (action.type) {
		case actions.LOGIN_USER:
			return {
				...state,
				user: {
					isAuth: true,
					name: action.payload.name,
					email: action.payload.email,
					role: action.payload.role,
					token: window.localStorage.getItem('result'),
				},
			};
		case actions.LOGOUT_USER:
			return { ...state, ...userInitialState };
		default:
			return state;
	}
}
