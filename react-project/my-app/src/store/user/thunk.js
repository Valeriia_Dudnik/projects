import { loginUserAction, logoutUserAction } from './actionCreators';
import { fetchLogUser, logoutUser } from '../../services';

export const logSomeUser = (logUser) => {
	return async function (dispatch) {
		await fetchLogUser(logUser).then((response) => {
			// console.log(response);
			const userToken = response.result;
			window.localStorage.setItem('result', userToken);
			dispatch(loginUserAction(response.user));
		});
	};
};

export const logoutSomeUser = () => {
	return async function (dispatch) {
		await logoutUser().then(() => {
			dispatch(logoutUserAction());
			window.localStorage.removeItem('result');
		});
	};
};
