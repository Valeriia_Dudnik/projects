import * as actions from './actionTypes';

const authorsInitialState = { authors: [] };

export default function authorReducer(state = authorsInitialState, action) {
	switch (action.type) {
		case actions.ADD_AUTHOR:
			return { ...state, authors: [...state.authors, action.payload] };
		case actions.GET_AUTHOR:
			return { ...state, authors: action.payload };
		default:
			return state;
	}
}
