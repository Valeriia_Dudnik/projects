import { addAuthorAction } from './actionCreators';
import { fetchNewAuthor } from '../../services';

export const addAuthor = (newAuthor) => {
	return async function (dispatch) {
		await fetchNewAuthor(newAuthor)
			.then((response) => {
				// console.log(response);
				dispatch(addAuthorAction(response.result));
			})
			.catch((error) => {
				console.error('There was an error!', error);
			});
	};
};
