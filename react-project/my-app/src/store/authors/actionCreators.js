import * as actions from './actionTypes';

export const addAuthorAction = (payload) => ({
	type: actions.ADD_AUTHOR,
	payload,
});

export const getAuthorsAction = (payload) => ({
	type: actions.GET_AUTHOR,
	payload,
});
