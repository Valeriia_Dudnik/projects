import { combineReducers, applyMiddleware } from 'redux';
import { createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import userReducer from './user/reducer';
import coursesReducer from './courses/reducer';
import authorReducer from './authors/reducer';

const rootReducer = combineReducers({
	user: userReducer,
	courses: coursesReducer,
	authors: authorReducer,
});

const saveState = (state) => {
	try {
		const serialisedState = JSON.stringify(state);
		window.localStorage.setItem('app_state', serialisedState);
	} catch (error) {
		console.error('There was an error!', error);
	}
};

const loadState = () => {
	try {
		const serialisedState = window.localStorage.getItem('app_state');
		if (!serialisedState) return undefined;
		return JSON.parse(serialisedState);
	} catch (err) {
		return undefined;
	}
};
const oldState = loadState();

const store = createStore(
	rootReducer,
	oldState,
	composeWithDevTools(applyMiddleware(thunk))
);

store.subscribe(() => {
	saveState(store.getState());
});

export default store;
