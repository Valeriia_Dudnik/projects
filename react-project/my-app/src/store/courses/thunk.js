import {
	addCourseAction,
	deleteCourseAction,
	updateCourseAction,
} from './actionCreators';

import {
	fetchNewCourse,
	deleteSomeCourse,
	updateSomeCourse,
} from '../../services';

export const addCourse = (newCourse) => {
	return async function (dispatch) {
		await fetchNewCourse(newCourse).then((response) => {
			// console.log(response);
			dispatch(addCourseAction(response.result));
		});
	};
};

export const deleteCourse = (courseID) => {
	return async function (dispatch) {
		await deleteSomeCourse(courseID).then(() => {
			dispatch(deleteCourseAction(courseID));
		});
	};
};

export const updateCourse = (courseID, course) => {
	return async function (dispatch) {
		await updateSomeCourse(courseID, course).then(() => {
			dispatch(updateCourseAction(courseID, course));
		});
	};
};
