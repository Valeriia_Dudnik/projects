import * as actions from './actionTypes';

const coursesInitialState = { courses: [] };

export default function coursesReducer(state = coursesInitialState, action) {
	switch (action.type) {
		case actions.GET_COURSES:
			return { ...state, courses: action.payload };
		case actions.ADD_COURSE:
			return { ...state, courses: [...state.courses, action.payload] };
		case actions.DELETE_COURSE:
			return {
				...state,
				courses: state.courses.filter((course) => course.id !== action.payload),
			};
		case actions.UPDATE_COURSE:
			return {
				...state,
				courses: state.courses.map((item) =>
					item.id === action.courseID ? action.course : item
				),
			};
		default:
			return state;
	}
}
