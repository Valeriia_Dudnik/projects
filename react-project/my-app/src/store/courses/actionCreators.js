import * as actions from './actionTypes';

export const addCourseAction = (payload) => ({
	type: actions.ADD_COURSE,
	payload,
});

export const deleteCourseAction = (payload) => ({
	type: actions.DELETE_COURSE,
	payload,
});

export const updateCourseAction = (courseID, course) => ({
	type: actions.UPDATE_COURSE,
	courseID,
	course,
});

export const getCoursesAction = (payload) => ({
	type: actions.GET_COURSES,
	payload,
});
