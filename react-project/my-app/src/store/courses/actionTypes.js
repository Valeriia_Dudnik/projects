export const ADD_COURSE = 'addCourse';
export const DELETE_COURSE = 'deleteCourse';
export const UPDATE_COURSE = 'updateCourse';
export const GET_COURSES = 'getCourses';
