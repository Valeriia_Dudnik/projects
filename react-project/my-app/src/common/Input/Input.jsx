import React from 'react';
import './input.css';
import PropTypes from 'prop-types';

function Input({
	value,
	onChange,
	placeholder,
	id,
	labelName,
	className = 'inputField',
	type = 'text',
}) {
	return (
		<div>
			<label htmlFor={id}>{labelName}</label>
			<input
				className={className}
				type={type}
				id={id}
				placeholder={placeholder}
				value={value}
				onChange={onChange}
			/>
		</div>
	);
}

Input.propTypes = {
	value: PropTypes.string,
};

export default Input;
