import React from 'react';
import './button.css';

export default function Button(props) {
	return (
		<button
			type='submit'
			className={props.className}
			onClick={props.clickEvent}
		>
			{props.text}
		</button>
	);
}
