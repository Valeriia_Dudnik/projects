export const button_logout = 'Logout';

export const button_login = 'Login';

export const button_addNewCourse = 'Add new Course';

export const button_showCourse = 'Show course';

export const button_createCourse = 'Create course';

export const button_updateCourse = 'Update course';

export const button_createAuthor = 'Create author';

export const button_add = 'Add';

export const button_delete = 'Delete';

export const button_search = 'Search';

export const button_registration = 'Registration';

export const button_goBack = '< Back to courses';
