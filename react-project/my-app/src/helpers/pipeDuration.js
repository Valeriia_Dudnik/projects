function pipeDuration(time) {
	let hours = Math.floor(time / 60);
	let minutes = time - hours * 60;
	if (isNaN(time) || time < 1) {
		return '00:00 hours';
	} else if (minutes < 10 && hours < 10) {
		return '0' + hours + ':0' + minutes + ' hours';
	} else if (hours < 10) {
		return '0' + hours + ':' + minutes + ' hours';
	} else if (minutes < 10) {
		return hours + ':0' + minutes + ' hours';
	} else {
		return hours + ':' + minutes + ' hours';
	}
}

export default pipeDuration;
