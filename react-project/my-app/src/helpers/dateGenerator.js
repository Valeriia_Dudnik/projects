function dateGenerator(date) {
	let dateSplit = date.split('/');
	let formatedData = [];
	for (let i = 0; i < dateSplit.length; i++) {
		if (dateSplit[i] < 10 && dateSplit[i].length < 2) {
			formatedData.push('0' + dateSplit[i]);
		} else {
			formatedData.push(dateSplit[i]);
		}
	}
	return formatedData.join('.');
}

export default dateGenerator;
