import React, { useEffect, useState } from 'react';
import './App.css';

import BoardComponent from "./components/BordComponent";
import LostFigures from './components/LostFigures';
import Timer from './components/Timer';
import { Board } from './models/Board';
import { Colors } from './models/Colors';
import { Player } from './models/Player';

function App() {
  const [board, setBoard] = useState(new Board())
  const [whitePlayer, setWhitePlayer] = useState(new Player(Colors.WHITE))
  const [blackPlayer, setBlackPlayer] = useState(new Player(Colors.BLACK))
  const [currentPlayer, setCurrtntPlayer] = useState<Player | null>(null)

  useEffect(() => {
    restart()
    setCurrtntPlayer(whitePlayer);
  }, [])

  function restart() {
    const newBoard = new Board();
    newBoard.initCells()
    newBoard.addFigures()
    setBoard(newBoard)
  }

  function swapPlayer() {
    setCurrtntPlayer(currentPlayer?.color === Colors.WHITE ? blackPlayer : whitePlayer)
  }

  return (
    <div className="app">
      <Timer 
        restart={restart}
        currentPlayer={currentPlayer}
      />
      <BoardComponent 
        board={board}
        setBoard={setBoard}
        currentPlayer={currentPlayer}
        swapPlayer={swapPlayer}
        restart={restart}
      />
      <div>
        <LostFigures 
          title='White figures' 
          figures={board.lostWhiteFigures} />
        <LostFigures 
          title='Black figures' 
          figures={board.lostBlackFigures} />
      </div>
    </div>
  );
}

export default App;
