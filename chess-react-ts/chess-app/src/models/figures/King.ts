import { Cell } from "../Cell";
import { Colors } from "../Colors";
import {Figure, FigureNames} from "./Figure";

const blackLogo =  require("../../ assets/black-king.png");
const whiteLogo =  require("../../ assets/white-king.png");

export class King extends Figure {

        constructor(color: Colors, cell: Cell) {
            super(color, cell);
            this.logo = color === Colors.BLACK ? blackLogo : whiteLogo
            this.name = FigureNames.KING
    }

    canMove(target: Cell): boolean {
        if(!super.canMove(target)) {
            return false
        }
        if((target.y === this.cell.y + 1 || target.y === this.cell.y - 1)
            && this.cell.isEmptyVertical(target)) {
            return true;
        }
        if((target.x === this.cell.x + 1 || target.x === this.cell.x - 1)
            && this.cell.isEmptyHorizontal(target)) {
            return true;
        }
        if((target.x === this.cell.x + 1 || target.x === this.cell.x - 1)
            && this.cell.isEmptyDiagonal(target)) {
            return true;
        }
        return false;
    }

}