import React, { FC, useEffect, useState } from 'react';
import { Board } from '../models/Board';
import { Cell } from '../models/Cell';
import { Colors } from '../models/Colors';
import { FigureNames } from '../models/figures/Figure';
import { Player } from '../models/Player';
import CellComponent from './CellCompoent';

interface BoardProps {
  board: Board;
  setBoard: (board: Board) => void;
  currentPlayer: Player | null;
  swapPlayer: () => void;
  restart: () => void;
}

const BordComponent: FC<BoardProps> = ({board, setBoard, currentPlayer, swapPlayer, restart}) => {

  const [selectedCell, setSelectedCell] = useState<Cell | null> (null);
 
  function click(cell: Cell) {
    if(selectedCell && selectedCell !== cell && selectedCell.figure?.canMove(cell)) {
      // selectedCell.moveFigure(cell);
      if(cell.figure?.name === FigureNames.KING) {
        selectedCell.moveFigure(cell);
        alert(`${currentPlayer?.color} player WON`);
        restart();
      } else {
          selectedCell.moveFigure(cell);
          swapPlayer();
          setSelectedCell(null);
      }
    } else {
      if(cell.figure?.color === currentPlayer?.color) {
        setSelectedCell(cell);
      }
    }
  }

  useEffect(() => {
    highlightCells()
  }, [selectedCell])

  function highlightCells() {
    board.highlightCells(selectedCell)
    updateBoard()
  }

  function updateBoard() {
    const newBoard = board.getCopyBoard()
    setBoard(newBoard)
  }

  return (
    <div>
      <div className="playerBox">
        <h3 className={currentPlayer?.color === Colors.WHITE ? "playerWhite" : "playerBlack"}>{currentPlayer?.color}</h3>
        <h4>-Player makes a moove</h4>
      </div>

      <div className="board">
        {board.cells.map((row, index) => 
          <React.Fragment key={index}>
            {row.map(cell => 
              <CellComponent
                cell={cell}
                key={cell.id}
                selected = {cell.x === selectedCell?.x && cell.y === selectedCell?.y}
                click={click}
              />
              )}
          </React.Fragment>
        )}
      </div>
    </div>
  );
}

export default BordComponent;
