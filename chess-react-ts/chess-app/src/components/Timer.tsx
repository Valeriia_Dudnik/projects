import React, { FC, useEffect, useRef, useState } from 'react';
import { Colors } from '../models/Colors';
import { Player } from '../models/Player';

interface TimerProps {
    currentPlayer: Player | null;
    restart: () => void;
}

const Timer: FC<TimerProps> = ({currentPlayer, restart}) => {
    const [whiteTime, setWhiteTime] = useState(300)
    const [blackTime, setBlackTime] = useState(300)
    const timer = useRef<null | ReturnType<typeof setInterval>>(null)

    useEffect(() => {
        startTimer()
    }, [currentPlayer])

    function startTimer() {
        if(timer.current) {
            clearInterval(timer.current)
        }
        const callback = currentPlayer?.color === Colors.WHITE ? decrementWhiteTimer : decrementBlackTimer
        timer.current = setInterval(callback, 1000)
    }

    const handleRestart = () => {
        setWhiteTime(300)
        setBlackTime(300)
        restart()
    }

    function decrementWhiteTimer() {
        setWhiteTime(prev => prev - 1)
    }

    function decrementBlackTimer() {
        setBlackTime(prev => prev - 1)
    }

    return (
        <div className="timersBack">
            <div>
                <h2>Timer:</h2>
                <hr/>
                <h3 className={currentPlayer?.color === Colors.WHITE ? "playerWhite" : ""}>WHITE - {whiteTime}</h3>
                <h3 className={currentPlayer?.color === Colors.BLACK ? "playerBlack" : ""}>BLACK - {blackTime}</h3>
                <hr/>
            </div>

            <button className="resButton" onClick={handleRestart}>Restart game</button>
        </div>
    );
};

export default Timer;