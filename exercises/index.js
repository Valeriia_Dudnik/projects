const compare = (ransomNote, magazine) => {
  let arr1 = [...magazine];
  for (const letter of ransomNote) {
    const index = arr1.indexOf(letter);
    if (index < 0) return false;
    arr1.splice(index, 1);
  }
  return true;
};
console.log(compare("aa", "ab"));
console.log(compare("aa", "aab"));



const sum = (arr) => {
  let count = [arr[0], ];
  for (let i=1; i<arr.length; i++) {
    let num = count[i-1] + arr[i];
    count.push(num);
  }
  return count;
};
arr = [3, 1, 2, 10, 4];
console.log(sum(arr));



const maxSum = (accounts) => {
  let sum = [];
  for (let i=0; i<accounts.length; i++) {
    let num = 0;
    for (let j=0; j<accounts[i].length; j++) {
      num = num + accounts[i][j];
    }
    sum.push(num);
  }
  return Math.max(...sum);
};
let accounts = [[7,1,3], [2,8,7], [1,9,5]];
console.log(maxSum(accounts));



function game(num) {
  let arr = [];
  for (let i=1; i<=num; i++) {
    if (i%3 == 0 && i%5 == 0 ) {
      arr.push("FizzBuzz");
    } else if (i%3 == 0) {
      arr.push("Fizz");
    } else if (i%5 == 0) {
      arr.push("Buzz");
    } else {
      arr.push(i);
    }
  }
  return arr;
}
console.log(game(5));
console.log(game(15));



function countStaps(num) {
  let count = 0;
  if (num != 0) {
    if (num%2 == 0) {
      num = num/2;
      count = count + 1;
      countStaps(num);
    } else {
      num = num - 1;
      countStaps(num);
    }
  }
  return count;
}
document.write(countStaps(14));



//Mumbling
function accum(s) {
	let arr = s.toUpperCase().split("")
  let arr2 = [];
  let z = 0;
  
  arr.forEach((letter) => {
      let str = "";
      for (let i=0; i<z; i++) {
        str = str + letter;
      }
      z = z+1
     arr2.push(letter + str.toLowerCase());
    })
  
  return arr2.join("-");
}



//Who likes it?
function likes(names) {
  if (names.length == 0) {
    return 'no one likes this';  
  } else if (names.length == 1){
    return names[0] + ' likes this';
  } else if (names.length == 2) {
    return names[0] + ' and ' + names[1] + ' like this';
  } else if (names.length == 3) {
    return  names[0] + ', ' + names[1] + ' and ' + names[2] + ' like this';
  } else {
    return names[0] + ', ' + names[1] + ' and ' + (names.length - 2) + ' others like this'
  }
}



//Highest Scoring Word
function high(sent){
  let arr = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"];
  let words = sent.split(" ");
  let maxCount = 0;
  let max;
  words.forEach((elem) => {
    let count = 0;
    for(let i=0; i<elem.length; i++) {
      count = count + arr.indexOf(elem[i]) + 1;
    }
    if (count>maxCount) {
      maxCount = count;
      max = elem;
    }
  })
  return max;
}


//The highest profit wins!
function minMax(arr){
  return [Math.min(...arr), Math.max(...arr)];
}



//Array.diff
function arrayDiff(a, b) {
    for(let i = 0; i < b.length; i++){
         if(a.includes(b[i])){
            a.splice(a.indexOf(b[i]), 1)
            arrayDiff(a, b)
        }
    }
    return a
}



//Combine objects
function combine() {
  let obj = {}

  for (let i = 0; i < arguments.length; i++) {
        for (let key in arguments[i]) {
          obj[key] = obj[key] ? obj[key] + arguments[i][key]: arguments[i][key]
        }
  }

  return obj;
}



//Get the Middle Character
function getMiddle(s)
{
  return s.substr(Math.ceil(s.length / 2 - 1), s.length % 2 === 0 ? 2 : 1);
}



//Smart Sum
function smartSum(...allArgs) {
  return allArgs.reduce((total, elem) => {
    if (Array.isArray(elem)) {
      return total + smartSum(...elem);
    } else {
      return total + elem;
    }
  }, 0);
}




//Trimming a string
function trim ( str, size ) {
  if ( str.length <= size ) {
    return str;
  }
  
  const real = str.slice(0, size);
  
  return `${real.length < 4 ? real : real.slice(0, -3)}...`;
}

