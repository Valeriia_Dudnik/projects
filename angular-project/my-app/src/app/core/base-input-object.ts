export const baseInputObject = {
    text: 'test:',
    labelFor: 'test',
    id: 'test',
    placeholder: 'test text',
    type: 'text',
    styles: {},
  }