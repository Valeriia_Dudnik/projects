import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

describe('AuthenticationService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthService);
  });
 
  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#login should log "logged in successfully", save user to local storage', () => {
    expect(service.login()).toBe('logged in successfully');
  });

  it('#login should log "Logout Lilu Dallas", delete user to local storage', () => {
    service.user = {
      id: '1a2b3c',
      firstName: 'Lilu',
      lastName: 'Dallas',
      token: 'token',
  } 
    expect(service.logout()).toBe('Logout Lilu Dallas');
  });

  it('#getUserInfo should return user name', () => {
    service.user = {
      id: '1a2b3c',
      firstName: 'Lilu',
      lastName: 'Dallas',
      token: 'token',
  } 
    expect(service.getUserInfo()).toBe('Lilu Dallas');
  });

  it('#setAuthenticated should set authenticated ', () => {
    expect(service.setAuthenticatedTrue()).toEqual(service.getIsAuthenticated());
  });

});
