import { Injectable } from "@angular/core";

@Injectable ({
    providedIn: 'root'
})

export class LoggerService {
    log(message: any): void {
        return console.log(message);
    }
}