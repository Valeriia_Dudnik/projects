import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { LoggerService } from './logger.service';
  
@Injectable({
  providedIn: 'root'
})

export class AuthService {
  private isAuthenticated: boolean = false;
  private auth: boolean = true;

  user: User = {
    id: '1a2b3c',
    firstName: 'User',
    lastName: 'Login',
    token: 'token',
  } 

  constructor(private readonly logger: LoggerService) { }

  getIsAuthenticated(): boolean {
    return this.isAuthenticated;
  }

  setAuthenticatedTrue(): boolean {
    return this.isAuthenticated = true;
  }

  setAuthenticatedFalse(): boolean {
    return this.isAuthenticated = false;
  }

  getAuth(): boolean {
    return this.auth;
  }

  setAuthFalse(): boolean {
    return this.auth = false;
  }

  setAuthTrue(): boolean {
    return this.auth = true;
  }

  login(): string {
    localStorage.setItem('user', JSON.stringify(this.user));
    this.logger.log('logged in successfully');

    return 'logged in successfully';
  }

  logout(): string {
    localStorage.removeItem('user');
    this.logger.log(`Logout ${this.user.firstName} ${this.user.lastName}`);

    return `Logout ${this.user.firstName} ${this.user.lastName}`;
  }
  
  getUserInfo(): string {
      return `${this.user.firstName} ${this.user.lastName}`;
  }

}
