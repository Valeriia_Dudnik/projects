export interface Button {
    styles: object,
    text: string,
    type: string,
    src?: string,
    disabled?: boolean,
}