export interface BaseInput {
    text: string,
    placeholder: string,
    labelFor: string,
    id: string,
    type?: string,
    styles: object,
    cols?: string,
    rows?: string,
    variableName?: string,
    variableNameNum?: number,
    variableNameDate?: Date,
}