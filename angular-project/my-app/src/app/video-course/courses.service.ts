import { Injectable } from '@angular/core';
import mockedCoursesList from '../shared/mock-courses-list';
import { Course } from '../core/interfaces/course';
import { Observable, of, map } from 'rxjs';

@Injectable()

export class CoursesService {
 private readonly courses: Observable<Course[]> = of(mockedCoursesList);

getCoursesList(): Observable<Course[]> {
  return this.courses;
}

create(): Observable<Course[]> {
  return this.courses;
}

getCourseById(id: string): Observable<Course[]> {
  return this.courses.pipe(
    map((coursesList: Course[]) => coursesList.filter(course => course.id === id))
  );
}

update(id: string): Observable<Course[]> {
  return this.getCourseById(id);
}

remove(id: string): Observable<Course[]> {
  return this.courses.pipe(
    map((coursesList: Course[]) => coursesList.filter(course => course.id !== id))
  );
}

}
