import { Component } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { Button } from 'src/app/core/interfaces/button';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent {
  userName: string = this.authService.getUserInfo();

  logOutBtnConfig: Button = {
    styles: {
      lineHeight: '24px',
      fontSize: '14px',
      backgroundColor: 'rgba(0, 0, 0, 0)',
      fontFamily: '"Source Sans Pro Regular", sans-serif',
      margin: '0',
      padding: '0',
      marginLeft: '8px',
      width: '68px',
    },
    text: 'Log out',
    type: 'button',
    src: 'assets/logOut.png',
  };

  logInBtnConfig: Button = {
    styles: {
      lineHeight: '24px',
      fontSize: '14px',
      backgroundColor: 'rgba(0, 0, 0, 0)',
      fontFamily: '"Source Sans Pro Regular", sans-serif',
      margin: '0',
      padding: '0',
      marginLeft: '8px',
      width: 'fit-content',
    },
    text: `${this.userName}`,
    type: 'button',
    src: 'assets/user.png',
  };

  constructor(
    private authService: AuthService,
    private router: Router, ) { 
  }

  get shouldAuth(): boolean  {
    return this.authService.getAuth();
  }

  clickedLogOut(): void {
    this.authService.logout();
    this.authService.setAuthenticatedFalse();
    this.authService.setAuthFalse();
    this.router.navigate(['/login']);
  };

  clickedLogIn(): void {
    this.authService.setAuthFalse();
    this.router.navigate(['/login']);
  }
}