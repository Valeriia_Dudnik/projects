import { Component, OnInit, OnDestroy } from '@angular/core';
import { LoggerService } from '../../core/services/logger.service';
import { CoursesService } from '../courses.service';
import { Course } from '../../core/interfaces/course';
import { Button } from 'src/app/core/interfaces/button';
import { Subject, map } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})

export class CoursesComponent implements OnInit, OnDestroy {
  destroy$: Subject<boolean> = new Subject<boolean>();
  coursesList: Course[] = [];
  inputQuery = '';

  imgBtnConfig: Button = {
    styles: {
      width: '111px',
      height: '36px',
      backgroundColor: '#009ECC',
      fontSize: '14px',
      letterSpacing: '-0.1px',
      float: 'right',
      marginBottom: '17px',
    },
    text: 'Add Course',
    type: 'button',
    src: 'assets/add.png',
  };

  loadMoreConfig: Button = {
    styles: {
      lineHeight: '24px',
      fontSize: '14px',
      backgroundColor: 'rgba(0, 0, 0, 0)',
      fontFamily: '"Source Sans Pro Regular", sans-serif',
      color: 'blue',
      margin: '0',
      padding: '0',
      textDecoration: 'underline',
      height: '24px',
	    width: 'fit-content',
    },
    text: 'Load more',
    type: 'button',
  };


  constructor(
    private readonly logger: LoggerService, 
    private coursesServis: CoursesService, 
    private router: Router,
    private route: ActivatedRoute, ) { 
    this.logger.log('constructor');
  }

  ngOnInit(): void {
    this.logger.log('OnInit');
    this.getCoursesList();
  }

  getCoursesList(): void {
    this.coursesServis.getCoursesList()
    .pipe(map(courses => this.coursesList = courses))
    .pipe(takeUntil(this.destroy$))
    .subscribe();
  }

  deleteSelectedCourse(id: string): void {
    let removeItemAgreement = prompt("Do you really want to delete this course?", "Yes");
    if (removeItemAgreement != null && removeItemAgreement.toLowerCase().trim() === 'yes') {
      this.coursesServis.remove(id)
      .pipe(map(courses => this.coursesList = courses))
      .pipe(takeUntil(this.destroy$))
      .subscribe();
    }
    this.logger.log(id);
  }

  setIputQuery(value: string): void {
    this.logger.log(value);
  } 
  
  trackCourseById(item: any): void {
    return item.id; 
  };

  loadMoreCourses(): void {
    this.logger.log('clicked - Load more');
  }

  editSelectedCourse(id: string): void {
    this.router.navigate([`/courses/${id}`]);
  }

  showSearchedCourses(value: string): void {
    this.inputQuery = value;
  }

  addNewCourse(): void {
    this.router.navigate(['/courses/new']);
  }

  ngOnDestroy(): void {
    this.logger.log('OnDestroy');
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
