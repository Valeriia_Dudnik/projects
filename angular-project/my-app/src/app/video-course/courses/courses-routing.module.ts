import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'src/app/core/services/auth-guard.service';
import { AddCourseComponent } from './add-course/add-course.component';
import { CoursesComponent } from './courses.component';

const courseRoutes: Routes = [
  { path: 'new', component: AddCourseComponent, canActivate: [AuthGuard] },
  { path: ':id', component: AddCourseComponent, canActivate: [AuthGuard] },
  { path: '', component: CoursesComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(courseRoutes)],
  exports: [RouterModule]
})

export class CourseRoutingModule { }