import { ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { CoursesComponent } from './courses.component';
import { CourseComponent } from './course/course.component';
import { ButtonComponent } from 'src/app/shared/button/button.component';
import { SearchComponent } from 'src/app/shared/search/search.component';

import { OrderByPipe } from '../../shared/pipes/order-by.pipe';
import { FilterPipe } from '../../shared/pipes/filter.pipe';
import { DurationPipe } from '../../shared/pipes/duration.pipe';

import { CoursesService } from '../courses.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('CoursesComponent', () => {
  let component: CoursesComponent;
  let componentCourse: CourseComponent;
  let componentButton: ButtonComponent;
  let componentSearch: SearchComponent;

  let fixture: ComponentFixture<CoursesComponent>;
  let fixtureCourse: ComponentFixture<CourseComponent>;
  let fixtureButton: ComponentFixture<ButtonComponent>;
  let fixtureSearch: ComponentFixture<SearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoursesComponent, CourseComponent, ButtonComponent, SearchComponent, OrderByPipe, FilterPipe, DurationPipe ],
      imports: [ FormsModule, RouterTestingModule ],
      providers: [ CoursesService ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(CoursesComponent);
    fixtureCourse = TestBed.createComponent(CourseComponent);
    fixtureButton = TestBed.createComponent(ButtonComponent);
    fixtureSearch = TestBed.createComponent(SearchComponent);

    component = fixture.componentInstance;
    componentCourse = fixtureCourse.componentInstance;
    componentButton = fixtureButton.componentInstance;
    componentSearch = fixtureSearch.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should log "load more" after click', fakeAsync(() => {
    console.log = jasmine.createSpy("log");
    let loadMore = fixture.debugElement.nativeElement.querySelector('.course_block_width_loadMore button');
    loadMore.click();
    expect(console.log).toHaveBeenCalledWith('clicked - Load more');
  }));

  it('should show - NO DATA, if there is no courses', () => {
    component.coursesList = [];
    fixture.detectChanges();

    let emptyListText = fixture.debugElement.nativeElement.querySelector('.course_block_width_emptyContainer');
    let text = 'No data. Feel free to add new courses';
    expect(emptyListText.textContent).toBe(text);
  });

  it('should return courses ordered by creation date', () => {
    component.coursesList = [
      {
      id: "1a-2b-3c",
      title: "Video Course 1. Name tag",
      creationDate: new Date("06/20/2022"),
      duration: 88,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
      },
      {
      id: "1a-2b-3c",
      title: "Video Course 2. Name tag",
      creationDate: new Date("08/28/2020"),
      duration: 2318,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
      },
    ];
    fixture.detectChanges();

    let expectedFirstDate = 'Aug 28, 2020';

    let coursesOrder = fixture.debugElement.nativeElement.querySelector('.course__block_time_creation p');
    expect(coursesOrder.textContent).toBe(expectedFirstDate);
  });

  it('should filter courses according to the search input', () => {
    component.inputQuery = '3';
    fixture.detectChanges();

    let courses = fixture.debugElement.nativeElement.querySelectorAll('.course');
    expect(courses.length).toBe(1);
  });

});
