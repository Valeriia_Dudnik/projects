import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Button } from 'src/app/core/interfaces/button';
import { LoggerService } from 'src/app/core/services/logger.service';
import mockedCoursesList from 'src/app/shared/mock-courses-list';
import { BaseInput } from 'src/app/core/interfaces/base-input';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-course',
  templateUrl: './add-course.component.html',
  styleUrls: ['./add-course.component.scss']
})

export class AddCourseComponent implements OnInit { 
  id: string;

  form: FormGroup;

  inputDurationConfig: BaseInput;
  inputDateConfig: BaseInput;
  inputAuthorConfig: BaseInput;

  cancelBtnConfig: Button;

  constructor(
    private readonly logger: LoggerService,
    private router: Router,
    private route: ActivatedRoute ) { }

    createId(): string {
      return Math.random().toString(36).slice(2);
    }

  ngOnInit(): void {
    this.form = new FormGroup({
      id: new FormControl(this.createId(), Validators.required),
      title: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      duration: new FormControl(null, Validators.required),
      creationDate: new FormControl(null, Validators.required),
      author: new FormControl(null, Validators.required),
    });

    this.id = this.route.snapshot.params['id'];

    if (this.id) {
      let course = mockedCoursesList.filter((course) => course.id === this.id);

      this.form.controls['id'].setValue(course[0].id);
      this.form.controls['title'].setValue(course[0].title);
      this.form.controls['description'].setValue(course[0].description);
      this.form.controls['duration'].setValue(course[0].duration);
      this.form.controls['creationDate'].setValue(course[0].creationDate);
      this.form.controls['author'].setValue(course[0].author);
    } 

    this.inputDurationConfig = {
      type: 'number',
      text: 'Duration:',
      labelFor: 'duration',
      id: 'duration',
      placeholder: 'minutes',
      styles: {
        width: '300px',
      },
    }

    this.inputDateConfig = {
      type: 'text',
      text: 'Date:',
      labelFor: 'date',
      id: 'date',
      placeholder: 'MM/DD/YYYY',
      styles: {
        width: '150px',
      },
    }

    this.inputAuthorConfig = {
      type: 'text',
      text: 'Authors:',
      labelFor: 'author',
      id: 'author',
      placeholder: 'Add author',
      styles: {
        width: '312px',
      },
    }  

    this.cancelBtnConfig = {
      styles: {
        width: '76px',
        height: '36px',
        backgroundColor: '#ced0db',
        fontSize: '14px',
        marginRight: '12px',
        color: '#5b616f',
      },
      text: 'Cancel',
      type: 'button',
  } 
}

  buttonCancelClicked(): void {
    this.logger.log('Cancel button clicked');
    this.router.navigate(['/courses']);
  }

  submitAddForm(): void {
    if(this.id) {
      let selectedCourse = mockedCoursesList.findIndex((course) => course.id === this.id);
      mockedCoursesList.splice(selectedCourse, 1, this.form.value);
    } else {
      mockedCoursesList.push(this.form.value);
    }
    this.router.navigate(['/courses']);
  }
}
