import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { CourseComponent } from './course.component';
import { CoursesComponent } from '../../courses/courses.component';
import { ButtonComponent } from 'src/app/shared/button/button.component';

import { OrderByPipe } from '../../../shared/pipes/order-by.pipe';
import { FilterPipe } from '../../../shared/pipes/filter.pipe';
import { DurationPipe } from '../../../shared/pipes/duration.pipe';

import { RouterTestingModule } from '@angular/router/testing';
import { CoursesService } from '../../courses.service';

describe('CourseComponent', () => {
  let component: CoursesComponent;
  let componentEl: CourseComponent;
  let componentButton: ButtonComponent;

  let fixture: ComponentFixture<CoursesComponent>;
  let fixtureEl: ComponentFixture<CourseComponent>;
  let fixtureButton: ComponentFixture<ButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CourseComponent, CoursesComponent, ButtonComponent, OrderByPipe, FilterPipe, DurationPipe ],
      imports: [ FormsModule, RouterTestingModule ],
      providers: [ CoursesService ],
    })
    
    .compileComponents();

    fixture = TestBed.createComponent(CoursesComponent);
    fixtureEl = TestBed.createComponent(CourseComponent);
    fixtureButton = TestBed.createComponent(ButtonComponent);

    component = fixture.componentInstance;
    componentEl = fixtureEl.componentInstance;
    componentButton = fixtureButton.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(componentEl).toBeTruthy();
  });

  it('should have img', () => {
    let creationImg = fixture.debugElement.nativeElement.querySelector('.course__block_time_creation img');
    expect(creationImg).toBeTruthy();

    let durationImg = fixture.debugElement.nativeElement.querySelector('.course__block_time_duration img');
    expect(durationImg).toBeTruthy();

    let editImg = fixture.debugElement.nativeElement.querySelector('.edit-button .btn-withImg img');
    expect(editImg).toBeTruthy();

    let deleteImg = fixture.debugElement.nativeElement.querySelector('.delete-button .btn-withImg img');
    expect(deleteImg).toBeTruthy();
  });

  it('should have button', () => {
    let buttonDelete = fixture.debugElement.nativeElement.querySelector('.delete-button .btn-withImg');
    expect(buttonDelete).toBeTruthy();

    let buttonEdit = fixture.debugElement.nativeElement.querySelector('.edit-button .btn-withImg');
    expect(buttonEdit).toBeTruthy();
  });

  it('should display correct title in upperCase', () => {
    let courseDe = fixture.debugElement.query(By.css('.course__block_title'));
    let courseEl = courseDe.nativeElement;
    let expectedCourse = {
      id: '1a-2b-3c',
      title: 'Video Course 2. Name tag',
      creationDate: new Date("06/20/2022"),
      duration: 88,
      description: `Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.`,
      };
      componentEl.course = expectedCourse;
      
    expect(courseEl.textContent).toBe(expectedCourse.title.toUpperCase());
  });

  it('should log id of course after delete button click', fakeAsync(() => {
    spyOn(componentEl, 'buttonDeleteClicked');
    console.log = jasmine.createSpy("log");
    fixture.detectChanges(); 

    let buttonDelete = fixture.debugElement.query(By.css('.delete-button .btn-withImg'))
    buttonDelete.triggerEventHandler('click', null);
    tick();
    fixture.detectChanges();
    expect(console.log).toHaveBeenCalledWith('2a-2b-2c');
  }));

  it('should have green border if course is fresh', () => {
    componentEl.course = {
      id: "1a-2b-3c",
      title: "Video Course 1. Name tag",
      creationDate: new Date("06/30/2022"),
      duration: 88,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
    };
    fixtureEl.detectChanges();

    let greenCourse = fixture.debugElement.query(By.css('.border-green'));
    expect(componentEl.courseBorderColor()).toBe('green');
    expect(greenCourse).toBeTruthy();
  });

  it('should have blue border if course is upcoming', () => {
    componentEl.course = {
      id: "1a-2b-3c",
      title: "Video Course 3. Name tag",
      creationDate: new Date("07/08/2023"),
      duration: 65,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
    };
    fixtureEl.detectChanges();

    let blueCourse = fixture.debugElement.query(By.css('.border-blue'));
    expect(componentEl.courseBorderColor()).toBe('blue');
    expect(blueCourse).toBeTruthy();
  });

  it('should be topRated - have a star and change background color', () => {
    componentEl.course = {
      id: "1a-2b-3c",
      title: "Video Course 3. Name tag",
      creationDate: new Date("07/08/2023"),
      duration: 65,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
    };
    fixtureEl.detectChanges();

    let backgroung = fixture.debugElement.query(By.css('.top-сourse'));
    let star = fixture.debugElement.query(By.css('.fa-star'));

    expect(componentEl.topRated).toBe(true);
    expect(backgroung).toBeTruthy();
    expect(star).toBeTruthy();
  });

  it('should return correct duration time', () => {
    component.coursesList = [{
      id: "1a-2b-3c",
      title: "Video Course 3. Name tag",
      creationDate: new Date("07/08/2023"),
      duration: 65,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
    }];
    fixture.detectChanges();

    let time = fixture.debugElement.nativeElement.querySelector('.course__block_time_duration p');
    expect(time.textContent).toBe('01h 05min');
  });

  it('should return correct creation date', () => {
    component.coursesList = [{
      id: "1a-2b-3c",
      title: "Video Course 3. Name tag",
      creationDate: new Date("07/08/2023"),
      duration: 65,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
    }];
    fixture.detectChanges();

    let date = fixture.debugElement.nativeElement.querySelector('.course__block_time_creation p');
    expect(date.textContent).toBe('Jul 8, 2023');
  });

});
