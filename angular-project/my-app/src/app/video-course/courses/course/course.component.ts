import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy  } from '@angular/core';
import { Course } from '../../../core/interfaces/course';
import { Button } from 'src/app/core/interfaces/button';
 
@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class CourseComponent {
  durationImg = 'assets/clock.png';
  creationImg = 'assets/calendar.png';

  currentDate = new Date();
  oldDate = new Date(new Date().setDate(this.currentDate.getDate() - 14));
  topRated = true;

  editBtnConfig: Button = {
    styles: {
      width: '60px',
      height: '24px',
      backgroundColor: '#009ecc',
      fontSize: '12px',
      letterSpacing: '-0.1px',
    },
    text: 'Edit',
    type: 'button',
    src: 'assets/pencil.png',
  };

  deleteBtnConfig: Button = {
    styles: {
      width: '72px',
      height: '24px',
      backgroundColor: '#009ecc',
      fontSize: '12px',
      letterSpacing: '-0.1px',
      marginLeft: '12px',
    },
    text: 'Delete',
    type: 'button',
    src: 'assets/delete.png',
  };

  @Input() course!: Course;
  @Output() deleteEventEmitter = new EventEmitter<string>();
  @Output() editEventEmitter = new EventEmitter<string>();

  buttonEditClicked(value: string): void {
    this.editEventEmitter.emit(value);
  }

  buttonDeleteClicked(value: string): void {
    this.deleteEventEmitter.emit(value);
  }

  courseBorderColor(): string {
    if ((this.course.creationDate < this.currentDate) &&
      (this.course.creationDate >= this.oldDate)) {
      return 'green';
    } else if (this.course.creationDate > this.currentDate) {
      return 'blue';
    } else {
      return 'grey';
    }
  }

}
