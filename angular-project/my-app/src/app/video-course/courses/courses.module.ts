import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthGuard } from "src/app/core/services/auth-guard.service";
import { InputAuthorsComponent } from "src/app/shared/input-authors/input-authors.component";
import { InputDateComponent } from "src/app/shared/input-date/input-date.component";
import { InputDurationComponent } from "src/app/shared/input-duration/input-duration.component";
import { SearchComponent } from "src/app/shared/search/search.component";
import { SharedModule } from "src/app/shared/shared-module";
import { CoursesService } from "../courses.service";
import { AddCourseComponent } from "./add-course/add-course.component";
import { CourseRoutingModule } from "./courses-routing.module";
import { CourseComponent } from "./course/course.component";
import { CoursesComponent } from "./courses.component";

@NgModule({
    declarations: [
        CoursesComponent,
        CourseComponent,
        AddCourseComponent,
        InputDateComponent,
        InputDurationComponent,
        InputAuthorsComponent,
        SearchComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CourseRoutingModule,
        SharedModule,
    ],
    providers: [
        CoursesService,
        AuthGuard,
    ],
})
export class CoursesModule {}