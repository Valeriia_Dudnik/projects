import { Component, Output, EventEmitter } from '@angular/core';
import { NgForm} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { LoggerService } from 'src/app/core/services/logger.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {
  email: string = '';
  pass: string = '';

  constructor(
    private authService: AuthService,
    private router: Router,
    private readonly logger: LoggerService ) { 
  }

  @Output() loginUserEmitter = new EventEmitter<object>();

  submitForm(form: NgForm): void {
    this.loginUserEmitter.emit(form);
    this.logger.log(form);
    this.authService.login();
    this.authService.setAuthenticatedTrue();
    this.authService.setAuthTrue();
    this.router.navigate(['/courses']);
  }

}
