import { TestBed } from '@angular/core/testing';
import mockedCoursesList from '../shared/mock-courses-list';

import { CoursesService } from './courses.service';

describe('CoursesService', () => {
  let service: CoursesService;

  beforeEach(() => {
    TestBed.configureTestingModule({ providers: [ CoursesService ] });
    service = TestBed.inject(CoursesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#getCoursesList should return courses list', () => {
    service.getCoursesList().subscribe(data => {
      expect(data).toEqual(mockedCoursesList);
    })
  });

  it('#removeItem should remove courses from list', () => {
    service.getCoursesList().subscribe();
    let expectedList = [{
      id: "2a-2b-2c",
      title: "Video Course 2. Name tag",
      creationDate: new Date("08/28/2020"),
      duration: 2318,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
      },
      {
      id: "3a-3b-3c",
      title: "Video Course 3. Name tag",
      creationDate: new Date("07/08/2023"),
      duration: 65,
      description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
      }]
    service.remove('1a-1b-1c').subscribe(data => {
      expect(data).toEqual(expectedList);
    })
  });

});
