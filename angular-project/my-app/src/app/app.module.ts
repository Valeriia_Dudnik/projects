import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './video-course/header/header.component';
import { FooterComponent } from './video-course/footer/footer.component';
import { LogoComponent } from './shared/logo/logo.component';
import { VideoCourseComponent } from './video-course/video-course.component';
import { LoginPageComponent } from './video-course/login-page/login-page.component';
import { NotFoundComponent } from './video-course/not-found/not-found.component';

import { UnlessDirective } from './shared/directives/unless.directive';
import { SharedModule } from './shared/shared-module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LogoComponent,
    VideoCourseComponent,
    UnlessDirective,
    LoginPageComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    SharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
