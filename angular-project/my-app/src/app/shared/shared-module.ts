import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ButtonComponent } from "./button/button.component";
import { DurationPipe } from "./pipes/duration.pipe";
import { FilterPipe } from "./pipes/filter.pipe";
import { OrderByPipe } from "./pipes/order-by.pipe";

@NgModule({
    declarations: [
        ButtonComponent,
        DurationPipe,
        OrderByPipe,
        FilterPipe,
    ],
    imports: [
        CommonModule,
    ],
    exports: [
        ButtonComponent,
        DurationPipe,
        OrderByPipe,
        FilterPipe,
    ],
})
export class SharedModule {}