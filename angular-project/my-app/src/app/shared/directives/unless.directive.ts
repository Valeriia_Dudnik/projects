import { Directive, Input, TemplateRef, ViewContainerRef }  from '@angular/core';

@Directive({
  selector: '[appUnless]'
})

export class UnlessDirective {
  private hasView = false;

  @Input() set appUnless(appUnless: boolean) {
    if (!appUnless && !this.hasView) {
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.hasView = true;
    } else if (appUnless && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef
    ) { }
}
