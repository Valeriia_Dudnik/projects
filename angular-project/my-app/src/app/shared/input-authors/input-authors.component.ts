import { Component, Input, OnDestroy } from '@angular/core';
import { ControlValueAccessor, FormControl, NG_VALUE_ACCESSOR } from '@angular/forms';
import { baseInputObject } from 'src/app/core/base-input-object';
import { BaseInput } from 'src/app/core/interfaces/base-input';

@Component({
  selector: 'app-input-authors',
  templateUrl: './input-authors.component.html',
  styleUrls: ['./input-authors.component.scss'],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: InputAuthorsComponent,
    multi: true
  }]
})

export class InputAuthorsComponent implements ControlValueAccessor, OnDestroy {
 
  @Input() inputConfig: BaseInput = baseInputObject;

  onChange: any = () => {}
  onTouch: any = () => {}

  val: FormControl<number> = new FormControl();
  subscriptions: any[] = [];


  set value(val: any) {
    if( val !== undefined && this.val !== val) {
      this.val = val
      this.onChange(val)
      this.onTouch(val)
    }
  }

  writeValue(value: any) {
    this.val.setValue(value);
  }

  registerOnChange(fn: any) {
    this.subscriptions.push(this.val.valueChanges.subscribe(fn))
  }

  registerOnTouched(fn: any) {
    this.onTouch = fn
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe())
  }
}