import { Component, Input, Output, EventEmitter } from '@angular/core';
import { baseButtonObject } from 'src/app/core/base-button-object';
import { Button } from 'src/app/core/interfaces/button';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})

export class ButtonComponent {

  @Input() buttonConfig: Button = baseButtonObject;
  @Output() buttonClickEmit = new EventEmitter();

  buttonClickEvent() {
		this.buttonClickEmit.emit();
	}
}

