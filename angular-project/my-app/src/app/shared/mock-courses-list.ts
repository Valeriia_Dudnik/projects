const mockedCoursesList = [
    {
    id: "1a-1b-1c",
    title: "Video Course 1. Name tag",
    creationDate: new Date("06/30/2022"),
    duration: 88,
    author: 'Elon Musk',
    description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
    },
    {
    id: "2a-2b-2c",
    title: "Video Course 2. Name tag",
    creationDate: new Date("08/28/2020"),
    duration: 2318,
    author: 'Sergey Brin',
    description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
    },
    {
    id: "3a-3b-3c",
    title: "Video Course 3. Name tag",
    creationDate: new Date("07/08/2023"),
    duration: 65,
    author: 'Larry Page',
    description: "Learn about where you can find course descriptions, what information they include, how they work, and details about various components of a course description. Course descriptions report information about a university or college's classes. They're published both in course catalogs that outline degree requirements and in course schedules that contain descriptions for all courses offered during a particular semester.",
    }
]
export default mockedCoursesList;