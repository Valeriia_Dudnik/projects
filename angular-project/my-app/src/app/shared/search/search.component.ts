import { Component, Output, EventEmitter } from '@angular/core';
import { Button } from 'src/app/core/interfaces/button';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {
  inputText = '';

  textBtnConfig: Button = {
    styles: {
      width: '73px',
      height: '36px',
      backgroundColor: '#67a300',
      fontSize: '14px',
      letterSpacing: '-0.008px',
      margin: '0 12px',
    },
    text: 'Search',
    type: 'button',
  };

  @Output() searchCourseEmitter = new EventEmitter<string>();

  searchCourse(): void {
    this.searchCourseEmitter.emit(this.inputText);
  }
}
