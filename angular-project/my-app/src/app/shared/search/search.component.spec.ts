import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { SearchComponent } from './search.component';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ButtonComponent } from '../button/button.component';

describe('SearchComponent', () => {
  let component: SearchComponent;
  let componentButton: ButtonComponent;

  let fixture: ComponentFixture<SearchComponent>;
  let fixtureButton: ComponentFixture<ButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchComponent, ButtonComponent ],
      imports: [ FormsModule ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SearchComponent);
    fixtureButton = TestBed.createComponent(ButtonComponent);

    component = fixture.componentInstance;
    componentButton = fixtureButton.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change inputText to input value', fakeAsync(() => {
    fixture.detectChanges();
    let input = fixture.debugElement.query(By.css(".box")).nativeElement;
    input.value = 'Angular';
    input.dispatchEvent(new Event('input'));

    fixture.whenStable().then(() => {
    let button = fixture.debugElement.nativeElement.querySelector('.searchButton');
    button.click();
    expect(component.inputText).toBe(input.value);
    })
  }));
});
