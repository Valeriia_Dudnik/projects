import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'duration'
})

export class DurationPipe implements PipeTransform {
    transform(time: number): string {
        let hours = Math.floor(time / 60);
        let minutes = time - hours * 60;

        if (isNaN(time) || time < 1) {
	        return '0 min';
    	} else if (minutes < 10 && hours < 10) {
        	return `0${hours}h 0${minutes}min`;
        } else if (hours < 10) {
	        return `0${hours}h ${minutes}min`;
    	} else if (minutes < 10) {
		    return `${hours}h ${minutes}min`;
	    } else {
	        return `${hours}h ${minutes}min`;
        }
    }
}
