import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false,
})

export class FilterPipe implements PipeTransform {
    transform(courses: any[], inputQuery: string): any {

        if (inputQuery.length > 0) {
          let searchedList = courses.filter((course) => 
          
          course.title.toLocaleLowerCase().includes(inputQuery.toLocaleLowerCase()));
          return searchedList;
        } else {
          return courses;
        }
    }
}
