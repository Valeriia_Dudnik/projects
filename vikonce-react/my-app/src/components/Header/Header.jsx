import React, { useState } from 'react';

import Facebook from '../../common/icons/facebook2.png';
import Instagram from '../../common/icons/instagram2.png';
import Phone from '../../common/icons/phone2.png';
import Logo from './icons/logo.png';
import User from './icons/user2.png';

// import { Link } from 'react-router-dom';
import { HashLink as Link } from 'react-router-hash-link';

import './header.css';

export default function Header() {
	const [showCall, setShowCall] = useState(false);
	const [numberToCall, setNumberToCall] = useState('+380');

	const callMeTrue = () => {
		setShowCall(true)
	}

	const callMeFalse = () => {
		setShowCall(false)
	}

	const [menu, setMenu] = useState(false);

	function openCloseMenu() {
		menu ? setMenu(false) : setMenu(true);
	}

	return (
		<header id="head">
			<div className="head">
				<div className="mobileMenu">
					<button className="menuClose" onClick={openCloseMenu}>
						<div className={menu ? 'line1Open line1' : 'line1'} ></div>
						<div className={menu ? 'line2Open line2' : 'line2'}></div>
					</button>
				
					{menu ? 
						<div className="menuList">
							<Link to="/gallery#top"  onClick={openCloseMenu}>
								ГАЛЕРЕЯ
							</Link>
							<Link to="/#services" onClick={openCloseMenu}>
								ПОСЛУГИ
							</Link>
							<Link to="/wallpaper#top" onClick={openCloseMenu}>
								ШПАЛЕРИ
							</Link>
							<Link to="/#contacts" onClick={openCloseMenu}>
								КОНТАКТИ
							</Link>
						</div>
						: <></>
						}
				
				</div>
				<div className="headBlock">
					<div className="telephone">
					<img src={Phone} alt="phone" className="phone" onClick={showCall ? callMeFalse : callMeTrue} />
						{(showCall) ?
						<form className="callMe" >
							<p>передзвонити вам?</p>
							<input type="text" size="15" placeholder="+380" id="personsNumber" className="personsNumber" 
							value={numberToCall}
							onChange={(e) => setNumberToCall(e.target.value)} />
							<button type="submit" className="sendButton" onClick={callMeFalse}>Відправити</button>
						</form>
						:
						<div className="number">
							<div>
								<p>+38(050) 188-21-31</p>
								<p>+38(097) 024-67-48</p>
							</div>
							<div>
								<p>ПН.-НД.:</p>
								<p>9<sup>00</sup>-18<sup>00</sup></p>
							</div>
						</div>}

					</div>
				</div>
				<div className="logo">
					<Link to="">
						<img src={Logo} alt="logo" />
					</Link>
				</div>
				<div className="social">

					<a href="https://www.instagram.com/vikonce_mrpl/">
						<img src={Instagram} alt="instagram"
						className="instagram" />
					</a>

					<a href="https://www.facebook.com/vikonce.spd">
						<img src={Facebook} alt="facebook" className="facebook" />		
					</a>

					<Link to="/login">
						<img src={User} alt="user" className="user_button" />
					</Link>
				</div>
			</div>
			<nav className="nav">
				<Link smooth to="/gallery">
					ГАЛЕРЕЯ
				</Link>
				<Link to="/#services">
					ПОСЛУГИ
				</Link>
				<Link to="/wallpaper">
					ШПАЛЕРИ
				</Link>
				<Link to="/#contacts">
					КОНТАКТИ
				</Link>
			</nav>
		</header>
	);
}