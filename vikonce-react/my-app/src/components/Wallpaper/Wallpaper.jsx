import React from 'react';

import Cat from './image/cat.jpg';

import './wallpaper.css';

const Wallpaper = () => {
    return(
        <div className="wall">
            <img src={Cat} alt="cat" />
            <h2>Нові шпалери<br />вже скоро будуть!</h2>
        </div>
    	);
};

export default Wallpaper;