import React, { useEffect } from 'react';

import { changes } from './bannerChange';

import './banner.css';

const Banner = () => {

    useEffect(
     changes
    , [])

    return (
        <div className="about" id="about">
      		<p className="aboutText">Дизайн-студія штор «Вiконце» відкриває Вам чудову можливість змінити зовнішній вигляд
      		Вашого будинку, зробивши інтер'єр стильним та цікавим!</p>

    		<div className="cCircle" id="circleChange">
                <div className="circle circle1" id="circle01"></div>
                <div className="circle" id="circle02"></div>
                <div className="circle" id="circle03"></div>
            </div>
    	    <div className="aboutImg" id="banner"></div>
		</div>
    )
}
export default Banner;
