export const changes = () => {
  setTimeout(() => {
    bannerSecond();
  }, 3000);
}

export const bannerSecond = () => {
  let banner = document.getElementById('banner');
  banner.classList.remove('aboutImg');
  banner.classList.add('aboutImg2');
  let circle01 = document.getElementById('circle01');
  circle01.classList.remove('circle1');
  let circle02 = document.getElementById('circle02');
  circle02.classList.add('circle2');
  setTimeout(() => {
    bannerThird();
  }, 3000)
}

export const bannerThird = () => {
  let banner = document.getElementById('banner');
  banner.classList.remove('aboutImg2');
  banner.classList.add('aboutImg3');
  let circle01 = document.getElementById('circle01');
  circle01.classList.remove('circle1');
  let circle02 = document.getElementById('circle02');
  circle02.classList.remove('circle2');
  let circle03 = document.getElementById('circle03');
  circle03.classList.add('circle3');
  setTimeout(() => {
    bannerFirst();
  }, 3000)
}

export const bannerFirst = () => {
  let banner = document.getElementById('banner');
  banner.classList.remove('aboutImg3');
  banner.classList.add('aboutImg');
  let circle03 = document.getElementById('circle03');
  circle03.classList.remove('circle3');
  let circle01 = document.getElementById('circle01');
  circle01.classList.add('circle1');
  setTimeout(() => {
    bannerSecond();
  }, 3000)
}

// export const change01 = () => {  
//   clearTimeout(t);
//   let banner = document.getElementById('banner');
//   banner.classList.remove('aboutImg2');
//   banner.classList.remove('aboutImg3');
//   banner.classList.add('aboutImg');
//   let circle02 = document.getElementById('circle02');
//   circle02.classList.remove('circle2');
//   let circle03 = document.getElementById('circle03');
//   circle03.classList.remove('circle3');
//   let circle01 = document.getElementById('circle01');
//   circle01.classList.add('circle1');
//   t = setTimeout(() => {
//     bannerSecond();
//   }, 3000);
// }

// export const change02 = () => {  
//   clearTimeout(t);
//   let banner = document.getElementById('banner');
//   banner.classList.remove('aboutImg');
//   banner.classList.remove('aboutImg3');
//   banner.classList.add('aboutImg2');
//   let circle01 = document.getElementById('circle01');
//   circle01.classList.remove('circle1');
//   let circle03 = document.getElementById('circle03');
//   circle03.classList.remove('circle3');
//   let circle02 = document.getElementById('circle02');
//   circle02.classList.add('circle2');
//   t = setTimeout(() => {
//     bannerThird();
//   }, 3000)
// }

// export const change03 = () => {  
//   clearTimeout(t);
//   let banner = document.getElementById('banner');
//   banner.classList.remove('aboutImg2');
//   banner.classList.remove('aboutImg');
//   banner.classList.add('aboutImg3');
//   let circle02 = document.getElementById('circle02');
//   circle02.classList.remove('circle2');
//   let circle01 = document.getElementById('circle01');
//   circle01.classList.remove('circle1');
//   let circle03 = document.getElementById('circle03');
//   circle03.classList.add('circle3');
//   t = setTimeout(() => {
//     bannerFirst();
//   }, 3000)
// }