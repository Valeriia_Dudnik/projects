import React from 'react';

import Design from '../icons/design-img3.jpg';
import Exclusive from '../icons/exclusive-design3.jpg';
import Him from '../icons/img2-him.jpg';
import { openRimska5, openRimska6, openRimska7, open2 } from './openWindowSecond';

import './servicesSecond.css';

const ServicesSecond = () => {

    return(
      <div id="windowOpen02" className="window02" onMouseOver={open2}>
        <div id="windowRimska02" className="rimskaOpen02">
          <div className="rimska02">
            <div className="service02"></div>
          </div>
          <div id="windowRimska5" className="rimskaOpen5 rimska5" onClick={openRimska5}>
            <div className="service5"></div>
            <p>пошиття</p>
          </div>
          <div id="windowRimska6" className="rimskaOpen6 rimska6" onClick={openRimska6}>
            <div className="service6"></div>
            <p>навіс і прасування</p>
          </div>
          <div id="windowRimska7" className="rimskaOpen7 rimska7" onClick={openRimska7}>
            <div className="service7"></div>
            <p>подальший догляд</p>
          </div>
        </div>
        <div className="aboutWindow02">
          <div className="aboutWindowWidth02">
            <div id="rimska5" className="aboutRimska5">
              <p>якісно та швидко<br />пошиття будь-якої складності</p>
              <img src={Design} alt="design" width="300px" />
            </div>
            <div id="rimska6" className="aboutRimska6">
              <p>один із головних етапів</p>
              <p className="aboutChild">Правильне навішування</p>
              <img src={Exclusive} alt="exclusive" width="300px" />
              <p className="pMobile">тільки коли будуть викладені усі складки</p>
              <p className="pMobile">ваші штори матимуть ідеальний вигляд</p>
            </div>
            <div id="rimska7" className="aboutRimska7">
              <p>хімчистка штор</p>
              <p>прання</p>
              <p>усунення дефектів</p>
              <img src={Him} alt="rulers" width="300px" />
            </div>
          </div>
        </div>

      </div>
    )
}
export default ServicesSecond;