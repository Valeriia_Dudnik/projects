export function openRimska5() {
  let a = document.getElementById('rimska5');
  if (getComputedStyle(a).display === 'none') {
    let b = document.getElementById('rimska6');
    let c = document.getElementById('rimska7');
    b.style.display = 'none';
    c.style.display = 'none';
    a.style.display = 'block';
  } else {
    a.style.display = 'none';
  }
}
export function openRimska6() {
  let b = document.getElementById('rimska6');
  if (getComputedStyle(b).display === 'none') {
    let a = document.getElementById('rimska5');
    let c = document.getElementById('rimska7');
    a.style.display = 'none';
    c.style.display = 'none';
    b.style.display = 'block';
  } else {
    b.style.display = 'none';
  }
}
export function openRimska7() {
  let c = document.getElementById('rimska7');
  if (getComputedStyle(c).display === 'none') {
    let a = document.getElementById('rimska5');
    let b = document.getElementById('rimska6');
    a.style.display = 'none';
    b.style.display = 'none';
    c.style.display = 'block';
  } else {
    c.style.display = 'none';
  }
}

export function open2 () {
  let a = document.getElementById('windowRimska5');
  a.classList.add('rimkaMoove5');
  let b = document.getElementById('windowRimska6');
  b.classList.add('rimkaMoove6');
  let c = document.getElementById('windowRimska7');
  c.classList.add('rimkaMoove7');
}