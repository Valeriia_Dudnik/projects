export function openRimska1() {
  let a = document.getElementById('rimska1');
  if (getComputedStyle(a).display === 'none') {
    let b = document.getElementById('rimska2');
    let c = document.getElementById('rimska3');
    let d = document.getElementById('rimska4');
    b.style.display = 'none';
    c.style.display = 'none';
    d.style.display = 'none';
    a.style.display = 'block';
  } else {
    a.style.display = 'none';
  }
}

export function openRimska2() {
  let b = document.getElementById('rimska2');
  if (getComputedStyle(b).display === 'none') {
    let a = document.getElementById('rimska1');
    let c = document.getElementById('rimska3');
    let d = document.getElementById('rimska4');
    a.style.display = 'none';
    c.style.display = 'none';
    d.style.display = 'none';
    b.style.display = 'block';
  } else {
    b.style.display = 'none';
  }
}

export function openRimska3() {
  let c = document.getElementById('rimska3');
  if (getComputedStyle(c).display === 'none') {
    let a = document.getElementById('rimska1');
    let b = document.getElementById('rimska2');
    let d = document.getElementById('rimska4');
    a.style.display = 'none';
    b.style.display = 'none';
    d.style.display = 'none';
    c.style.display = 'block';
  } else {
    c.style.display = 'none';
  }
}

export function openRimska4() {
  let d = document.getElementById('rimska4');
  if (getComputedStyle(d).display === 'none') {
    let a = document.getElementById('rimska1');
    let b = document.getElementById('rimska2');
    let c = document.getElementById('rimska3');
    a.style.display = 'none';
    b.style.display = 'none';
    c.style.display = 'none';
    d.style.display = 'block';
  } else {
    d.style.display = 'none';
  }
}

export function open() {
  let a = document.getElementById('windowRimska1');
  a.classList.add('rimkaMoove1');
  let b = document.getElementById('windowRimska2');
  b.classList.add('rimkaMoove2');
  let c = document.getElementById('windowRimska3');
  c.classList.add('rimkaMoove3');
  let d = document.getElementById('windowRimska4');
  d.classList.add('rimkaMoove4');
}
