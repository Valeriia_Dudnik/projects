import React, { useState } from 'react';

import Rulers from '../icons/rulers.jpg';
import Drow from '../icons/drow.jpg';
import Tkani from '../icons/tkani.jpg';
import { openRimska1, openRimska2, openRimska3, openRimska4, open } from './openWindowFirst';

import './servicesFirst.css';
import Button from '../../../../../common/Button/Button';
import { button_send } from '../../../../../common/Button/constants';
import { HashLink as Link } from 'react-router-hash-link';

const ServicesFirst = () => {
  const [userForm, setUserForm] = useState({
    name: '',
    number: '',
    adress: '',
    date: '',
    time: '',
  })

  const requiredFields = () => {
		alert('Будь ласка, заповніть усі поля!');
	};

  const clickSend = (e) => {
		e.preventDefault();
		if (Object.values(userForm).some((el) => el.length === 0)) {
			requiredFields();
		} 
	};

  const [clicked, setClicked] = useState(false);
  function openRimska11() {
    openRimska1();
    setClicked(clicked ? false : true);
  }

    return(
      <div>
      <div id="windowOpen" className="window">
        <div className="windowWidth" id='services' onMouseOver={open}>

          <div id="windowRimska" className="rimskaOpen">
            <div className="rimska0">
              <div className="service0"></div>
            </div>
            <Link to="/#space">
            <div id="windowRimska1" className="rimskaOpen1 rimska1" onClick={openRimska11}>
              <div className="service1"></div>
              <p>виїзд дизайнера</p>
            </div>
            </Link>
            <div id="windowRimska2" className="rimskaOpen2 rimska2" onClick={openRimska2}>
              <div className="service2"></div>
              <p>виконання вимірів</p>
            </div>
            <div id="windowRimska3" className="rimskaOpen3 rimska3" onClick={openRimska3}>
              <div className="service3"></div>
              <p>створення ескізів</p>
            </div>
            <div id="windowRimska4" className="rimskaOpen4 rimska4" onClick={openRimska4}>
              <div className="service4"></div>
              <p>підбір тканин<br />та фурнітури</p>
            </div>
          </div>

          <div className="aboutWindow">
            <div className="aboutWindowWidth">

              <div id="rimska1" className="aboutRimska1">
                <p className="pMobile">ми виїжджаємо додому,<br />навіть до інших міст!</p>
                <p>заповніть форму<br />і ми з вами зв'яжемося</p>
                <form id="formBlock" onSubmit={clickSend}>
                  <input type="text" 
                    size="30" 
                    className="textForm" 
                    placeholder="ПІБ" 
                    id="personsName" 				
                    value={userForm.name}
                    onChange={(e) => setUserForm({ ...userForm, name: e.target.value })} />
                  <input type="text"
                    className="textForm" 
                    placeholder="телефон +380" 
                    id="personsNumberForm"
                    value={userForm.number}
                    onChange={(e) => setUserForm({ ...userForm, number: e.target.value })}
                    maxLength="14" />
                    <br />
                  <input type="text" 
                    size="30" 
                    className="textForm" 
                    placeholder="адресса" 
                    id="personsAdress"
                    value={userForm.adress}
                    onChange={(e) => setUserForm({ ...userForm, adress: e.target.value })} />
                    <br />
                  <label htmlFor="personsDate">Дата виїзду:</label>
                  <label htmlFor="personsTime">Час виїзду:</label>
                  <input type="date" 
                    id="personsDate" 
                    name="personsDate" 
                    className="dateForm" 
                    min="2022-27-07" 
                    max="2022-12-31"
                    value={userForm.date}
                    onChange={(e) => setUserForm({ ...userForm, date: e.target.value })} />
                  <select className="dateForm" id="personsTime" name="personsTime"
                    value={userForm.time}
                    onChange={(e) => setUserForm({ ...userForm, time: e.target.value })}>
                      <option defaultValue>9:00</option>
                      <option>10:00</option>
                      <option>11:00</option>
                      <option>12:00</option>
                      <option>13:00</option>
                      <option>14:00</option>
                      <option>15:00</option>
                      <option>16:00</option>
                      <option>17:00</option>
                      <option>18:00</option>
                  </select>
                  {/* <input type="submit" value="Отправить" className="buttonForm" id="sendButtonForm" /> */}
                  <Button text={button_send} />
                </form>
              </div>

              <div id="rimska2" className="aboutRimska2">
                <p>виконуємо виміри будь-якої складності</p>
                <p>будь-яких виробів швидко та точно</p>
                <img src={Rulers} alt="rulers" width="300px" />
              </div>
              <div id="rimska3" className="aboutRimska3">
                <p>виконуємо ескізи будь-якої складності</p>
                <p>строго за вашими розмірами</p>
                <img src={Drow} alt="drow" width="300px" />
              </div>
              <div id="rimska4" className="aboutRimska4">
                <p>підбираємо тканини, що ідеально підходять до вашого інтер'єру.</p>
                <p>послуга перегляду тканин на дому</p>
                <img src={Tkani} alt="tkani" width="300px" />
              </div>
            </div>
          </div>
        </div>
      </div>
        
      {clicked ? <div className="moreSpace" id="space"></div> : <></>}
        </div>
    )
}
export default ServicesFirst;