import React from 'react';

import './about.css';

const About = () => {

    return(
        <div className="aboutUs">
            <div className="aboutCell">
                <div className="cell5"></div>
                <p>величезний вибір тканин, що постійно збільшується</p>
            </div>
            <div className="aboutCell">
                <div className="cell4"></div>
                <p>виїзд додому, робота з іншими містами</p>
            </div>
            <div className="aboutCell">
                <div className="cell3"></div>
                <p>Індивідуальний підхід до кожного клієнта</p>
            </div>
            <div className="aboutCell">
                <div className="cell1"></div>
                <p>наш безцінний досвід Ваш неповторний дизайн</p>
            </div>
            <div className="aboutCell">
                <div className="cell2"></div>
                <p>максимально доступні ціни</p>
            </div>
        </div>
    )
}
export default About;