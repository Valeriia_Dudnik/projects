import React from 'react';

import Banner from './components/Banner/Banner';
import About from './components/About/About';
import ServicesFirst from './components/Services/ServicesFirst/ServicesFirst';
import ServicesSecond from './components/Services/ServicesSecond/ServicesSecond';

import './mainPage.css';

const MainPage = () => {
	return (
		<>
			<Banner />
			<About />
			<ServicesFirst />
			<p className="someAbout">Наш салон штор пропонує чудовий вибір колекцій портьєрних та тюлевих тканин, різні
				види сонцезахисних систем, карнизів та аксесуарів.<br />
				Штори на вікнах - це останній штрих, без якого навіть в розкішно і гармонійно
				обставленому житло, не буде відчуття повного затишку та комфорту.
			</p>
			<ServicesSecond />
			<p className="someAbout">Бажання облаштувати своє житло зі смаком вимагає використання індивідуального підходу,
				якісної продукції,<br />
				обліку всіх особливостей Вашого характеру, стилю та моди.<br />
				Підбираючи стилістичні та колірні рішення тканин у
				інтер'єрі, важливо звернутися до знаючого спеціалісту.<br />
				Оригінальний, спеціально розроблений для Вас дизайн перетворить Ваше житло до невпізнання та
				допоможе здійснити Вашу мрію.
			</p>
		</>
	);
};

export default MainPage;
