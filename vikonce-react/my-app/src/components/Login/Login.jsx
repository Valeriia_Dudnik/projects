import React, { useState } from 'react';

import { Link } from 'react-router-dom';
// import { useNavigate } from 'react-router-dom';
// import { useDispatch } from 'react-redux';

import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import {button_login} from '../../common/Button/constants';


// import { logSomeUser } from '../../store/user/thunk';

import './login.css';

const Login = () => {
	// let navigate = useNavigate();
	// const dispatch = useDispatch();

	const requiredFields = () => {
		alert('Будь ласка, заповніть усі поля!');
	};

	const [logUser, setLogUser] = useState({
		email: '',
		password: '',
	});

	// const [error, setError] = useState('');

	// const loginSomeUser = () => {
	// 	dispatch(logSomeUser(logUser))
	// 		.then(() => {
	// 			setError('');
	// 		})
	// 		.catch((error) => {
	// 			setError(`This email doesn't exist or password is wrong`);
	// 			console.error('There was an error!', error);
	// 		});
	// };

	const clickLogin = (e) => {
		e.preventDefault();
		if (Object.values(logUser).some((el) => el.length === 0)) {
			requiredFields();
		} else {
			// loginSomeUser();
		}
	};

	return (
		<form className='logForm' onSubmit={clickLogin}>
			<h2>УВІЙТИ ДО ПЕРСОНАЛЬНОЇ СТОРІНКИ:</h2>
			<Input
				id='logEmail'
				placeholder='Введіть поштову скриньку'
				labelName='Поштова скринька'
				type='email'
				value={logUser.email}
				onChange={(e) => setLogUser({ ...logUser, email: e.target.value })}
			/>
			<Input
				id='logPassword'
				placeholder='Введіть пароль'
				labelName='Пароль'
				type='password'
				value={logUser.password}
				onChange={(e) => setLogUser({ ...logUser, password: e.target.value })}
			/>
			{/* <div className='log'>{error}</div> */}

			<br />
			<Button text={button_login} />
			<p className="regLink">
				Якщо Ви не маєте персональної сторінки,<br /> Ви можете створити її{' '}
				<Link to='/registration'>тут</Link>
			</p>
		</form>
	);
};

export default Login;
