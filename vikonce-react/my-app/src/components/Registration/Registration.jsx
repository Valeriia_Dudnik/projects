import React, { useState } from 'react';

import Button from '../../common/Button/Button';
import Input from '../../common/Input/Input';
import { button_registration } from '../../common/Button/constants.js';

import { Link } from 'react-router-dom';
// import { useNavigate } from 'react-router-dom';

// import { registerNewUser } from '../../services';

import './registration.css';

const Registration = () => {
	// let navigate = useNavigate();

	const requiredFields = () => {
		alert('Будь ласка, заповніть усі поля!');
	};

	const [newUser, setNewUser] = useState({ name: '', email: '', password: '' });

	// const [errorR, setErrorR] = useState('');

	// const postNewUser = () => {
	// 	registerNewUser(newUser)
	// 		.then(() => {
	// 			setErrorR('');
	// 			navigate('/login');
	// 		})
	// 		.catch((error) => {
	// 			setErrorR(`There is a problem: email already exists or
	// 				password length should be 6 characters minimum`);
	// 			console.error('There was an error!', error);
	// 		});
	// };

	const addNewUser = (e) => {
		e.preventDefault();
		if (Object.values(newUser).some((el) => el.length === 0)) {
			requiredFields();
		} else {
			// postNewUser();
		}
	};

	return (
		<form className='regForm' onSubmit={addNewUser}>
			<h2>СТВОРИТИ ПЕРСОНАЛЬНУ СТОРІНКУ:</h2>

			<Input
				id='regName'
				placeholder="Введіть Ваше Ім'я"
				labelName="Ім'я"
				value={newUser.name}
				onChange={(e) => setNewUser({ ...newUser, name: e.target.value })}
			/>

			<Input
				id='regEmail'
				placeholder='Введіть поштову скриньку'
				labelName='Поштова скринька'
				type='email'
				value={newUser.email}
				onChange={(e) => setNewUser({ ...newUser, email: e.target.value })}
			/>

			<Input
				id='regPassword'
				placeholder='Введіть пароль'
				labelName='Пароль'
				type='password'
				value={newUser.password}
				onChange={(e) => setNewUser({ ...newUser, password: e.target.value })}
			/>

			{/* <div className='reg'>{errorR}</div> */}

			<br />

			<Button text={button_registration} />

			<p className="logLink">
				Якщо у Вас вже є персональна сторінка,<br />Ви можете увійти до неї{' '}
                <Link to='/login'>тут</Link>
			</p>
		</form>
	);
};

export default Registration;
