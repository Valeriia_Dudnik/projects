import React from 'react';

import { HashLink as Link } from 'react-router-hash-link';

import './galleryCategory.css';

const GalleryCategory = (props) => {
    
    const photo = require(`../../../../common/images/covers/${props.name}`);

    return(
        <div className="photoFrame">
            <Link to={`/gallery/${props.urlName}`}>
                <img src={photo} alt="bedroom" width="250" height="250" />
                <h2>{props.category}</h2>
            </Link>
        </div>
    	);
};

export default GalleryCategory;