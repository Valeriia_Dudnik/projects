import React from 'react';
import GalleryCategory from './components/GalleryCategory/GalleryCategory';
import covers from '../../common/photoNamesJson/covers.json';

import './gallery.css';

const Gallery = () => {
    const names = covers.map((name) => {
        const [urlName, category] = name.split('_');
        const [categoryName,] = category.split('.');
        return [urlName, categoryName, name]
    })

    return(
        <div className="galleryBlock">
            {names.map((name) => <GalleryCategory 
                category={name[1]} 
                urlName={name[0]} 
                name={name[2]} 
                key={name[0]} 
                />)}
        </div>

    	);
};

export default Gallery;