import React from 'react';

import Facebook from '../../common/icons/facebook2.png';
import Instagram from '../../common/icons/instagram2.png';
import Phone from '../../common/icons/phone2.png';
import Place from './icons/place2.png'
import Up from './icons/up.png'
import Mail from './icons/mail2.png'
import User from './icons/user2.png';

import { Link } from 'react-router-dom';

import './footer.css';
 
export default function Footer() {
	return (
		<footer id="contacts">
			<div className="footer">
				<div className="footer_phone">
					<img src={Phone} alt="phone" />
					<div className="footer_number">
						<p>+38(050) 188-21-31</p>
						<p>+38(097) 024-67-48</p>
					</div>
				</div>
				<div className="footer_mail">
					<img src={Mail} alt="mail" />
					<p>VIKONCE-SPD@UKR.NET</p>
				</div>
				<div className="footer_place">
					<img src={Place} alt="place" />
					<p>м. Київ<br />Софієвська Борщагівка</p>
				</div>
				<a href="https://www.instagram.com/vikonce_mrpl/" className="footer_instagram">
					<img src={Instagram} alt="instagram" />
				</a>
				<a href="https://www.facebook.com/vikonce.spd" className="footer_facebook">
					<img src={Facebook} alt="facebook" />
				</a>
				<Link to="/login" className="user_log">
					<img src={User} alt="user"/>
				</Link>
			</div>
			<div className="footer_goUp">
				<a href="#head">
					<img src={Up} alt="up" />
				</a>
			</div>
		</footer>
	);
}