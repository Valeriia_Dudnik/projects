import React from 'react';
import { Routes, Route } from 'react-router-dom';

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import MainPage from './components/MainPage/MainPage';
import Gallery from './components/Gallery/Gallery';
import Wallpaper from './components/Wallpaper/Wallpaper';

import './App.css';
import CategoryPhotos from './components/Gallery/components/CategoryPhotos/CategoryPhotos';

function App() {

	return (
		<div className='app'>

			<Header />
			<main className='main'>
				<Routes>
					<Route path='/registration' element={<Registration />} />
					<Route path='/login' element={<Login />} />
					<Route exact path='/gallery' element={<Gallery />} />
					<Route exact path='/gallery/:category' element={<CategoryPhotos />} />
					<Route path='/wallpaper' element={<Wallpaper />} />

					<Route path='*' element={<MainPage />} />
				</Routes>
			</main>

			<Footer />
		</div>
	);
};

export default App;
