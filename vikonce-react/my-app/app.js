const fs = require('fs');

const covers= fs.readdirSync('./src/common/images/covers');
fs.writeFile('./src/common/photoNamesJson/covers.json', JSON.stringify(covers), 'utf8', function(err) {
    console.log(err);
});

const imagesBedroom = fs.readdirSync('./src/common/images/gallery/bedroom');
fs.writeFile('./src/common/photoNamesJson/imagesBedroom.json', JSON.stringify(imagesBedroom), 'utf8', function(err) {
    console.log(err);
});

const imagesChildrensRoom = fs.readdirSync('./src/common/images/gallery/childrensRoom');
fs.writeFile('./src/common/photoNamesJson/imagesChildrensRoom.json', JSON.stringify(imagesChildrensRoom), 'utf8', function(err) {
    console.log(err);
});

const imagesDiningRoom = fs.readdirSync('./src/common/images/gallery/diningRoom');
fs.writeFile('./src/common/photoNamesJson/imagesDiningRoom.json', JSON.stringify(imagesDiningRoom), 'utf8', function(err) {
    console.log(err);
});

const imagesPillow = fs.readdirSync('./src/common/images/gallery/pillow');
fs.writeFile('./src/common/photoNamesJson/imagesPillow.json', JSON.stringify(imagesPillow), 'utf8', function(err) {
    console.log(err);
});

const imagesTableclothСover = fs.readdirSync('./src/common/images/gallery/tableclothСover');
fs.writeFile('./src/common/photoNamesJson/imagesTableclothСover.json', JSON.stringify(imagesTableclothСover), 'utf8', function(err) {
    console.log(err);
});

const imagesCabinet = fs.readdirSync('./src/common/images/gallery/cabinet');
fs.writeFile('./src/common/photoNamesJson/imagesCabinet.json', JSON.stringify(imagesCabinet), 'utf8', function(err) {
    console.log(err);
});
